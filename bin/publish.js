#!/usr/bin/env node

/*
	Script to publish packages to NPM.
*/

const path = require('path');
const { spawnSync } = require('child_process');
const isCi = require('is-ci');

if(!isCi) {
	console.error('Should only publish from CI pipeline.');
	process.exit(1);
}

const ROOT_DIR = path.dirname(__dirname);
const PACKAGE_DIR = path.join(ROOT_DIR, 'packages');

const rootPackage = 'ecce';
const dependentPackages = [ 'ecce-preact', 'ecce-react' ];


/** @type { (packageName: string) => string } */
const getPublishedVersion = packageName => {
	const { status, stdout } = spawnSync('yarn', [ 'npm', 'info', '--fields', 'version', '--json', packageName ]);	

	if(status !== 0) {
		process.exit(1);
	}
	
	return JSON.parse(stdout.toString()).version;
}

/** @type { (packageName: string) => string } */
const getLocalVersion = packageName => {
	const version = require(path.join(PACKAGE_DIR, packageName, 'package.json')).version;
	if(!version) {
		throw new Error('No version found in local package ' + packageName);
	}

	return version;
}

/** @type { (packageName: string) => boolean } */
const willPublish = packageName => {
	const localVersion = getLocalVersion(packageName);
	const publishedVersion = getPublishedVersion(packageName);

	return !publishedVersion || localVersion !== publishedVersion;
}

const willPublishRoot = willPublish(rootPackage);

const errors = [];
const dependentsToPublish = dependentPackages.map(package => {
	const willPublishDependent = willPublish(package);
	if(willPublishRoot && !willPublishDependent) {
		errors.push(`Cannot publish new version of ${rootPackage} without also publishing dependant ${package}`);
		return null;
	}

	return willPublishDependent ? package : null;
})
	.filter(package => !!package);

if(errors.length) {
	console.error('Failed to publish:')
	errors.forEach(err => console.log(' - ', err));
	process.exit(1);
}

if(!willPublishRoot && !dependentsToPublish.length) {
	console.error('Nothing to publish.');
	process.exit(1);
}

console.log('Will publish:')
if(willPublishRoot) {
	console.log(' - ', rootPackage);
}
dependentsToPublish.forEach(package => console.log(' - ', package))

if(!process.env.NPM_AUTH_TOKEN) {
	console.error('No auth token.');
	process.exit(1);
}

/** @type { (packageName: string) => boolean } */
const publishPackage = packageName => {
	console.log(`\nPublishing ${packageName}:`);
	const { status } = spawnSync('yarn', [ 'workspace', packageName, 'npm', 'publish' ], {
		stdio: 'inherit'
	});
	
	return status === 0;
}

if(willPublishRoot) {
	if(!publishPackage(rootPackage)) {
		process.exit(1);
	}
}

dependentsToPublish.forEach(publishPackage);