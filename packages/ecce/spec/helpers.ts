export type SpyObserver = {
	observer: VoidFunction;
	numCalls: () => number;
	wasCalled: () => boolean;
};
export const spyObserver = (): SpyObserver => {
	let numCalls = 0;

	return {
		observer: () => { ++numCalls; },
		numCalls: () => numCalls,
		wasCalled: () => !!numCalls,
	};
};