import { describe, expect, it } from 'vitest';
import { observable } from '../src';
import { EcceError } from '../src/error';
import { typeName } from '../src/util';



describe('[ecce] @observable invalid location', () => {
	it('throws on construction if applied to a method', () => {
		class Foo {
			// @ts-expect-error Invalid location.
			@observable()
			bar() {
				//
			}
		}

		expect(() => {
			new Foo();
		})
			.to.throw(EcceError, `Cannot use @observable() on a method: \`${typeName(Foo)}.bar\``);
	});

	it('throws on construction if applied to a field', () => {
		class Foo {
			// @ts-expect-error Invalid location.
			@observable()
			bar = 5;
		}

		expect(() => {
			new Foo();
		})
			.to.throw(EcceError, `Cannot use @observable() on a field: \`${typeName(Foo)}.bar\`. Use an auto-accessor instead: \`@observable() accessor bar\``);
	});
});