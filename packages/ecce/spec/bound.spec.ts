import { describe, expect, it } from 'vitest';
import { bound } from '../src';
import { EcceError } from '../src/error';
import { typeName } from '../src/util';


describe('[ecce] bound()', () => {
	it('throws on construction if applied to a private member', () => {
		class Foo {
			// @ts-expect-error Invalid target
			@bound()
			#bar() { /**/ }
		}

		expect(() => new Foo())
			.to.throw(EcceError, `Cannot apply @bound() to a #private method: \`${typeName(Foo)}.#bar()\``);
	});

	it('binds a method to the instance of the class', () => {
		class Foo {
			#bar = 5;

			@bound()
			bar() {
				return this.#bar;
			}
		}

		const { bar } = new Foo();

		expect(bar())
			.to.eq(5);
	});

	it('binds method on different instances', () => {
		class Foo {
			#bar: number;
			constructor(bar: number) {
				this.#bar = bar;
			}

			@bound()
			bar() {
				return this.#bar;
			}
		}

		const { bar: barA } = new Foo(5);
		const { bar: barB } = new Foo(10);

		expect(barA())
			.to.eq(5);

		expect(barB())
			.to.eq(10);
	});
});