import { describe, expect, it } from 'vitest';
import { notify, observable, observe } from '../src';
import { EcceError } from '../src/error';
import { typeName } from '../src/util';
import { spyObserver } from './helpers';


describe('[ecce] @observable() getter', () => {
	it('can access a #private member', () => {
		class Foo {
			#bar = 5;

			@observable()
			get bar() {
				return this.#bar;
			}
		}

		expect(new Foo())
			.to.have.property('bar', 5);
	});

	it('can be observed via a manual call to notify()', () => {
		class Foo {
			@observable()
			get bar() {
				return 5;
			}
		}

		const foo = new Foo();

		const { observer, wasCalled } = spyObserver();

		observe(foo, 'bar', observer);

		notify(foo, 'bar');

		expect(wasCalled())
			.to.be.true;
	});

	it('can observe different members separately', () => {
		class Foo {
			@observable()
			get bar() {
				return 5;
			}

			@observable()
			get baz() {
				return 6;
			}
		}

		const foo = new Foo();

		const { observer: barObserver, numCalls: numBarCalls } = spyObserver();
		const { observer: bazObserver, numCalls: numBazCalls } = spyObserver();

		observe(foo, 'bar', barObserver);
		observe(foo, 'baz', bazObserver);

		notify(foo, 'bar');
		expect(numBarCalls())
			.to.eq(1);

		expect(numBazCalls())
			.to.eq(0);

		notify(foo, 'baz');
		expect(numBarCalls())
			.to.eq(1);

		expect(numBazCalls())
			.to.eq(1);
	});

	it('can observe different instances separately', () => {
		class Foo {
			@observable()
			get bar() {
				return 5;
			}
		}

		const { observer: aObserver, numCalls: numACalls } = spyObserver();
		const { observer: bObserver, numCalls: numBCalls } = spyObserver();

		const fooA = new Foo();
		observe(fooA, 'bar', aObserver);
		const fooB = new Foo();
		observe(fooB, 'bar', bObserver);

		notify(fooA, 'bar');

		expect(numACalls())
			.to.eq(1);
		expect(numBCalls())
			.to.eq(0);

		notify(fooB, 'bar');

		expect(numACalls())
			.to.eq(1);
		expect(numBCalls())
			.to.eq(1);
	});

	it('notifies owner', () => {
		class Foo {
			@observable()
			get bar() {
				return 5;
			}
		}

		const foo = new Foo();

		const { observer, wasCalled } = spyObserver();
		observe(foo, observer);

		notify(foo, 'bar');

		expect(wasCalled())
			.to.be.true;
	});

	it('throws on construction when decorating a private member', () => {
		class Foo {
			// @ts-expect-error Invalid location.
			@observable()
			get #bar() {
				return 5;
			}
		}

		expect(() => new Foo())
			.to.throw(EcceError, `Cannot use @observable() on a private getter: \`${typeName(Foo)}.#bar\``);
	});

	it('throws on decoration when applied to a static member', () => {
		expect(() => {
			class Foo { // eslint-disable-line @typescript-eslint/no-unused-vars
				// @ts-expect-error Invalid location.
				@observable()
				static get bar() {
					return 5;
				}
			}
		})
			.to.throw(EcceError, 'Cannot use @observable() on a static getter:');
	});
});