import { describe, expect, it } from 'vitest';
import { observable, observe } from '../src';
import { EcceError } from '../src/error';
import { spyObserver } from './helpers';


describe('[ecce] @observable setter', () => {
	it('can set a #private member', () => {
		class Foo {
			#bar = 5;

			get bar() {
				return this.#bar;
			}

			@observable()
			set bar(value: number) {
				this.#bar = value;
			}
		}

		const foo = new Foo();
		foo.bar = 10;

		expect(foo)
			.to.have.property('bar', 10);
	});

	it('is settable over different instances', () => {
		class Foo {
			value = 5;

			@observable()
			set bar(nextValue: number) {
				this.value = nextValue;
			}
		}

		const fooA = new Foo();
		const fooB = new Foo();

		fooA.bar = 10;
		fooB.bar = 20;

		expect(fooA)
			.to.have.property('value', 10);

		expect(fooB)
			.to.have.property('value', 20);
	});

	it('can be observed for mutation', () => {
		class Foo {
			#bar = 5;

			get bar() {
				return this.#bar;
			}

			@observable()
			set bar(value: number) {
				this.#bar = value;
			}
		}

		const foo = new Foo();

		const { observer, wasCalled } = spyObserver();

		observe(foo, 'bar', observer);

		foo.bar = 10;

		expect(wasCalled())
			.to.be.true;
	});

	it('can be observed for mutation on #private member', () => {
		class Foo {
			#_bar = 5;

			get bar() {
				return this.#_bar;
			}

			@observable()
			set #bar(value: number) {
				this.#_bar = value;
			}

			updateBar(value: number) {
				this.#bar = value;
			}
		}

		const foo = new Foo();

		const { observer, wasCalled } = spyObserver();

		observe(foo, 'bar', observer);

		foo.updateBar(10);

		expect(wasCalled())
			.to.be.true;
	});

	it('can be observed for mutation on Symbol member', () => {
		const key = Symbol();

		class Foo {
			#bar = 5;

			get [key]() {
				return this.#bar;
			}

			@observable()
			set [key](value: number) {
				this.#bar = value;
			}
		}

		const foo = new Foo();

		const { observer, wasCalled } = spyObserver();

		observe(foo, key, observer);

		foo[key] = 10;

		expect(wasCalled())
			.to.be.true;
	});

	it('can observe different members separately', () => {
		class Foo {
			#bar = 5;
			#baz = 6;

			get bar() {
				return this.#bar;
			}

			@observable()
			set bar(value: number) {
				this.#bar = value;
			}

			get baz() {
				return this.#baz;
			}

			@observable()
			set baz(value: number) {
				this.#baz = value;
			}
		}

		const foo = new Foo();

		const { observer: barObserver, numCalls: numBarCalls } = spyObserver();
		const { observer: bazObserver, numCalls: numBazCalls } = spyObserver();

		observe(foo, 'bar', barObserver);
		observe(foo, 'baz', bazObserver);

		foo.bar = 10;
		expect(numBarCalls())
			.to.eq(1);

		expect(numBazCalls())
			.to.eq(0);

		foo.baz = 10;
		expect(numBarCalls())
			.to.eq(1);

		expect(numBazCalls())
			.to.eq(1);
	});


	it('can observe different instances separately', () => {
		class Foo {
			#bar = 5;

			get bar() {
				return this.#bar;
			}

			@observable()
			set bar(value: number) {
				this.#bar = value;
			}
		}

		const { observer: aObserver, numCalls: numACalls } = spyObserver();
		const { observer: bObserver, numCalls: numBCalls } = spyObserver();

		const fooA = new Foo();
		observe(fooA, 'bar', aObserver);
		const fooB = new Foo();
		observe(fooB, 'bar', bObserver);

		fooA.bar = 10;

		expect(numACalls())
			.to.eq(1);
		expect(numBCalls())
			.to.eq(0);

		fooB.bar = 10;

		expect(numACalls())
			.to.eq(1);
		expect(numBCalls())
			.to.eq(1);
	});

	it('notifies owner on set', () => {
		class Foo {
			#bar = 5;

			get bar() {
				return this.#bar;
			}

			@observable()
			set bar(value: number) {
				this.#bar = value;
			}
		}

		const foo = new Foo();

		const { observer, wasCalled } = spyObserver();
		observe(foo, observer);

		foo.bar = 10;

		expect(wasCalled())
			.to.be.true;
	});

	it('throws on decoration when applied to a static member', () => {
		expect(() => {
			class Foo { // eslint-disable-line @typescript-eslint/no-unused-vars
				// @ts-expect-error Invalid location.
				@observable()
				static accessor bar = 5;
			}
		})
			.to.throw(EcceError, 'Cannot use @observable() on a static accessor:');
	});
});