import { describe, expect, it } from 'vitest';
import { isClassInstance, isPropertyKey, typeName } from '../src/util';


describe('[ecce] utils', () => {
	describe('typeName()', () => {
		const expectations: [ string, any ][] = [
			[ 'number', 5 ],
			[ 'number', NaN ],
			[ 'boolean', false ],
			[ 'boolean', true ],
			[ 'string', '"a string"' ],
			[ 'symbol', Symbol() ],
			[ 'array', [] ],
			[ 'null', null ],
			[ 'undefined', undefined ],
			[ 'Object', {} ],
			[ 'function', () => {/**/} ],
			[ 'function', function() { /**/ } ],
			[ 'getFoo', function getFoo() { /**/ } ],
		];

		expectations.forEach(([ expectation, value ]) => {
			it(`returns "${expectation}" for ${String(value)}`, () => {
				expect(typeName(value))
					.to.eq(expectation);
			});
		});

		it('returns Object for object with no prototype', () => {
			expect(typeName(Object.create(null)))
				.to.eq('Object');
		});

		it('returns class name for a class', () => {
			class Foo { /**/ }

			expect(typeName(Foo))
				.to.eq('Foo');
		});

		it('returns class name for a class instance', () => {
			class Foo { /**/ }

			expect(typeName(new Foo()))
				.to.eq('Foo');
		});
	});

	describe('isClassInstance()', () => {
		it('returns false for a undefined', () => {
			expect(isClassInstance(undefined))
				.to.be.false;
		});

		it('returns false for a null', () => {
			expect(isClassInstance(5))
				.to.be.false;
		});
		it('returns false for a primitive', () => {
			expect(isClassInstance(5))
				.to.be.false;
		});

		it('returns false for plain object', () => {
			expect(isClassInstance({}))
				.to.be.false;
		});

		it('returns false for an Array', () => {
			expect(isClassInstance([]))
				.to.be.false;
		});

		it('returns false for a class constructor', () => {
			class Foo {}

			expect(isClassInstance(Foo))
				.to.be.false;
		});

		it('returns true for a class instance', () => {
			class Foo {}

			expect(isClassInstance(new Foo()))
				.to.be.true;
		});
	});

	describe('isPropertyKey()', () => {
		class Foo {}
		const cases = [
			[ 'number', 5, true ],
			[ 'string', 'test', true ],
			[ 'symbol', Symbol(), true ],
			[ 'object', {}, false ],
			[ 'array', [] , false ],
			[ 'class instance', new Foo(), false ],
			[ 'null', null, false ],
			[ 'undefined', undefined, false ],
		] as const;

		cases.forEach(([ type, value, expectation ]) => {
			it(`returns ${expectation} for ${type}`, () => {
				expect(isPropertyKey(value))
					.to.eq(expectation);
			});
		});
	});
});