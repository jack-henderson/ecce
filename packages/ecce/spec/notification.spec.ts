import { describe, expect, it } from 'vitest';
import { notify, observable, observe } from '../src';
import { EcceError } from '../src/error';
import { typeName } from '../src/util';
import { spyObserver } from './helpers';


describe('[ecce] notification', () => {
	it('notifies immediate observers synchronously', () => {
		class Foo {
			@observable()
			get bar(): number {
				return 5;
			}
		}

		const foo = new Foo();
		const { observer, wasCalled } = spyObserver();

		observe(foo, 'bar', observer);

		notify(foo, 'bar');

		expect(wasCalled())
			.to.be.true;
	});

	it('notifies immediate observers multiple times', () => {
		class Foo {
			@observable()
			get bar(): number {
				return 5;
			}
		}

		const foo = new Foo();
		const { observer, numCalls } = spyObserver();

		observe(foo, 'bar', observer);

		notify(foo, 'bar');
		notify(foo, 'bar');
		notify(foo, 'bar');

		expect(numCalls())
			.to.eq(3);
	});

	it('throws for recursive observers', () => {
		class Foo {
			@observable()
			accessor bar = 5;

			@observable()
			accessor baz = 5;
		}

		const foo = new Foo();

		const barObserver = () => {
			foo.baz = 10;
		};
		observe(foo, 'bar', barObserver);

		const bazObserver = () => {
			foo.bar = 10;
		};

		observe(foo, 'baz', bazObserver);

		expect(() => {
			foo.bar = 20;
		})
			.to.throw(EcceError, `Recursive observers: ${typeName(Foo)}.bar -> ${typeName(Foo)}.baz -> ${typeName(Foo)}.bar`);
	});
});