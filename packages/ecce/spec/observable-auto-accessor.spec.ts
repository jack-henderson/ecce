import { describe, expect, it } from 'vitest';
import { observable, observe } from '../src';
import { EcceError } from '../src/error';
import { typeName } from '../src/util';
import { spyObserver } from './helpers';


describe('[ecce] @observable auto-accessor', () => {
	it('is gettable with initial value', () => {
		class Foo {
			@observable()
			accessor bar = 5;
		}

		expect(new Foo())
			.to.have.property('bar', 5);
	});

	it('is settable', () => {
		class Foo {
			@observable()
			accessor bar = 5;
		}

		const foo = new Foo();
		foo.bar = 10;

		expect(foo)
			.to.have.property('bar', 10);
	});

	it('is settable over different instances', () => {
		class Foo {
			@observable()
			accessor bar = 5;
		}

		const fooA = new Foo();
		const fooB = new Foo();

		fooA.bar = 10;
		fooB.bar = 20;

		expect(fooA)
			.to.have.property('bar', 10);

		expect(fooB)
			.to.have.property('bar', 20);
	});

	it('can be observed for mutation', () => {
		class Foo {
			@observable()
			accessor bar = 5;
		}

		const foo = new Foo();

		const { observer, wasCalled } = spyObserver();

		observe(foo, 'bar', observer);

		foo.bar = 10;

		expect(wasCalled())
			.to.be.true;
	});

	it('can observe different members separately', () => {
		class Foo {
			@observable()
			accessor bar = 5;

			@observable()
			accessor baz = 6;
		}

		const foo = new Foo();

		const { observer: barObserver, numCalls: numBarCalls } = spyObserver();
		const { observer: bazObserver, numCalls: numBazCalls } = spyObserver();

		observe(foo, 'bar', barObserver);
		observe(foo, 'baz', bazObserver);

		foo.bar = 10;
		expect(numBarCalls())
			.to.eq(1);

		expect(numBazCalls())
			.to.eq(0);

		foo.baz = 10;
		expect(numBarCalls())
			.to.eq(1);

		expect(numBazCalls())
			.to.eq(1);
	});

	it('can observe different instances separately', () => {
		class Foo {
			@observable()
			accessor bar = 5;
		}

		const { observer: aObserver, numCalls: numACalls } = spyObserver();
		const { observer: bObserver, numCalls: numBCalls } = spyObserver();

		const fooA = new Foo();
		observe(fooA, 'bar', aObserver);
		const fooB = new Foo();
		observe(fooB, 'bar', bObserver);

		fooA.bar = 10;

		expect(numACalls())
			.to.eq(1);
		expect(numBCalls())
			.to.eq(0);

		fooB.bar = 20;

		expect(numACalls())
			.to.eq(1);
		expect(numBCalls())
			.to.eq(1);
	});

	it('notifies owner on set', () => {
		class Foo {
			@observable()
			accessor bar = 5;
		}

		const foo = new Foo();

		const { observer, wasCalled } = spyObserver();
		observe(foo, observer);

		foo.bar = 10;

		expect(wasCalled())
			.to.be.true;
	});


	it('throws on construction when decorating a private member', () => {
		class Foo {
			// @ts-expect-error Invalid location.
			@observable()
			accessor #bar = 5;
		}

		expect(() => new Foo())
			.to.throw(EcceError, `Cannot use @observable() on a private accessor: \`${typeName(Foo)}.#bar\``);
	});

	it('throws on decoration when applied to a static member', () => {
		expect(() => {
			class Foo { // eslint-disable-line @typescript-eslint/no-unused-vars
				// @ts-expect-error Invalid location.
				@observable()
				static accessor bar = 5;
			}
		})
			.to.throw(EcceError, 'Cannot use @observable() on a static accessor:');
	});
});