import { describe, expect, it } from 'vitest';
import { observable, observe, unobserve } from '../src';
import { spyObserver } from './helpers';


describe('[ecce] unobserve()', () => {
	it('removes an immediate observer', () => {
		class Foo {
			@observable()
			accessor bar = 5;
		}

		const foo = new Foo();

		const { observer, numCalls } = spyObserver();

		observe(foo, 'bar', observer);

		foo.bar = 10;

		unobserve(foo, 'bar', observer);

		foo.bar = 20;

		expect(numCalls())
			.to.eq(1);
	});
});