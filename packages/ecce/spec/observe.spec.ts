import { describe, expect, it } from 'vitest';
import { observable, observe } from '../src';
import { EcceError } from '../src/error';
import { typeName } from '../src/util';


describe('[ecce] observe()', () => {
	it('throws if target is null', () => {
		expect(() => {
			// @ts-expect-error Cannot be null
			observe(null, () => { /**/ });
		})
			.to.throw(TypeError, 'Invalid target for observe(): target is null');
	});

	it('throws if target is undefined', () => {
		expect(() => {
			// @ts-expect-error Cannot be null
			observe(undefined, () => { /**/ });
		})
			.to.throw(TypeError, 'Invalid target for observe(): target is undefined');
	});

	it('throws if target is not an object', () => {
		expect(() => {
			// @ts-expect-error Invalid type
			observe(5, () => { /**/ });
		})
			.to.throw(TypeError, 'Invalid target for observe(): `5` is not an object');
	});

	it('throws if target is a class with no @observable() decorators', () => {
		class Foo {}

		expect(() => {
			observe(new Foo(), () => { /**/ });
		})
			.to.throw(EcceError, `Cannot observe() a class with no @observable() decorators: \`${typeName(Foo)}\``);
	});

	it('throws if target is a plain old javascript object', () => {
		expect(() => {
			observe({}, () => { /**/ });
		})
			.to.throw(EcceError, 'Cannot observe() an object not created with makeObservable()');
	});

	it('throws if key does not exist on target', () => {
		@observable()
		class Foo {}

		expect(() => {
			// @ts-expect-error Invalid key
			observe(new Foo(), 'bar', () => { /**/ });
		})
			.to.throw(EcceError, `Cannot observe() a non-existant property: \`${typeName(Foo)}.bar\``);
	});

	it('throws if key is not decorated with @observable()', () => {
		@observable()
		class Foo {
			accessor bar = 5;
		}

		expect(() => {
			observe(new Foo(), 'bar', () => { /**/ });
		})
			.to.throw(EcceError, `Cannot observe() a property not decorated with @observable(): \`${typeName(Foo)}.bar\``);
	});
});