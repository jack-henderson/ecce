import { describe, expect, it } from 'vitest';
import { notify, observable , observableRef } from '../src';


describe('[ecce] observableRef()', () => {
	it('throws if target is undefined', () => {
		// @ts-expect-error Cannot be undefined
		expect(() => observableRef(undefined))
			.to.throw('Invalid target for observableRef(): target is undefined');
	});

	it('throws if target is null', () => {
		// @ts-expect-error Cannot be null
		expect(() => observableRef(null))
			.to.throw('Invalid target for observableRef(): target is null');
	});


	it('returns a referentially equal value when called twice with no notification', () => {
		@observable()
		class Foo {}

		const foo = new Foo();

		expect(observableRef(foo))
			.to.eq(observableRef(foo));
	});

	it('returns a referentially unequal value after notifying', () => {
		@observable()
		class Foo {}

		const foo = new Foo();

		const preRef = observableRef(foo);
		notify(foo);

		expect(observableRef(foo))
			.to.not.eq(preRef);
	});
});