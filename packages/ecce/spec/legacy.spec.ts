import { describe, expect, it } from 'vitest';
import { callback, makeSubject, subject } from '../src';
import { makeObservable } from '../src/make-observable';


describe('[ecce] legacy', () => {
	describe('@subject', () => {
		it('does nothing', () => {
			@subject()
			class Foo {}

			expect(new Foo())
				.to.be.instanceof(Foo);
		});
	});

	describe('@callback', () => {
		it('binds a method', () => {
			class Foo {
				@callback
				bar() {
					return this;
				}
			}

			const foo = new Foo();
			const bar = foo.bar;

			expect(bar())
				.to.eq(foo);
		});
	});

	describe('makeSubject()', () => {
		it('is a synonym for makeObservable()', () => {
			expect(makeSubject)
				.to.eq(makeObservable);
		});
	});
});