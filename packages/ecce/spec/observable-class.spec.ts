import { describe, expect, it } from 'vitest';
import { notify, observable, observe } from '../src';
import { spyObserver } from './helpers';


describe('[ecce] @observable class', () => {
	it('is an instance of the decorated class', () => {
		@observable()
		class Foo {}

		expect(new Foo())
			.to.be.instanceof(Foo);
	});

	it('can be observed via manual notification', () => {
		@observable()
		class Foo {}

		const foo = new Foo();

		const { observer, wasCalled } = spyObserver();

		observe(foo, observer);

		notify(foo);

		expect(wasCalled())
			.to.be.true;
	});

	it('can observe different instances separately', () => {
		@observable()
		class Foo {}

		const { observer: aObserver, numCalls: numACalls } = spyObserver();
		const { observer: bObserver, numCalls: numBCalls } = spyObserver();

		const fooA = new Foo();
		observe(fooA, aObserver);
		const fooB = new Foo();
		observe(fooB, bObserver);

		notify(fooA);

		expect(numACalls())
			.to.eq(1);
		expect(numBCalls())
			.to.eq(0);

		notify(fooB);

		expect(numACalls())
			.to.eq(1);
		expect(numBCalls())
			.to.eq(1);
	});
});