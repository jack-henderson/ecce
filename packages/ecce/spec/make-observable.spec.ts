import { describe, expect, it } from 'vitest';
import { makeObservable, observe } from '../src';
import { EcceError } from '../src/error';
import { spyObserver } from './helpers';


describe('[ecce] makeObservable()', () => {
	it('returns a shallow cloned object', () => {
		const source = {
			foo: 5,
			bar: { baz: 10 },
		};

		const observable = makeObservable(source);

		expect(observable)
			.to.not.eq(source);

		expect(observable)
			.to.deep.eq(source);

		expect(observable.bar)
			.to.eq(source.bar);
	});

	it('can notify a property observer', () => {
		const observable = makeObservable({ foo: 5 });

		const { observer, wasCalled } = spyObserver();
		observe(observable, 'foo', observer);

		observable.foo = 10;
		expect(wasCalled())
			.to.be.true;
	});

	it('can notify whole object observer', () => {
		const observable = makeObservable({ foo: 5 });

		const { observer, wasCalled } = spyObserver();
		observe(observable, observer);

		observable.foo = 10;
		expect(wasCalled())
			.to.be.true;
	});

	it('throws when setting a non-existant property', () => {
		const observable = makeObservable<{foo: number, bar?: string}>({ foo: 5 });

		expect(() => {
			observable.bar = 'nope';
		})
			.to.throw(EcceError, 'Cannot set unknown property: `Object.bar`. Ensure all properties are set when calling makeObservable()');
	});

	it('sets name option', () => {
		const observable = makeObservable<{ foo: number, bar?: string }>({ foo: 5 }, { name: 'Test!' });

		expect(() => {
			observable.bar = 'nope';
		})
			.to.throw(EcceError, 'Cannot set unknown property: `Test!.bar`. Ensure all properties are set when calling makeObservable()');
	});
});