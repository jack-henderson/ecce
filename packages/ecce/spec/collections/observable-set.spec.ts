import { describe, expect, it } from 'vitest';
import { ObservableSet } from '../../src';
import { spyObserver } from '../helpers';
import { testRef } from './test-ref';


describe('[ecce] ObservableSet', () => {
	describe('add()', () => {
		it('returns false and does not notify if set already contains value', () => {
			const s = new ObservableSet<number>([ 5 ]);

			const { observer, numCalls } = spyObserver();
			s.observe(observer);

			expect(s.add(5))
				.to.be.false;

			expect(numCalls())
				.to.eq(0);
		});

		it('returns true notifies when value is added to set', () => {
			const s = new ObservableSet<number>([ 5 ]);

			const { observer, numCalls } = spyObserver();
			s.observe(observer);

			expect(s.add(6))
				.to.be.true;

			expect(numCalls())
				.to.eq(1);

			expect(s.has(6))
				.to.be.true;
		});
	});

	describe('addAll()', () => {
		it('returns false and does not notify when all values are already present', () => {
			const s = new ObservableSet<number>([ 5, 6, 7, 8 ]);

			const { observer, numCalls } = spyObserver();
			s.observe(observer);

			expect(s.addAll([ 5, 6, 7, 8 ]))
				.to.be.false;

			expect(numCalls())
				.to.eq(0);
		});

		it('returns true and notifies when some values are absent ', () => {
			const s = new ObservableSet<number>([ 5, 6, 7, 8 ]);

			const { observer, numCalls } = spyObserver();
			s.observe(observer);

			expect(s.addAll([ 7, 8, 9, 10 ]))
				.to.be.true;

			expect(s.toArray())
				.to.deep.eq([ 5, 6, 7, 8, 9, 10 ]);

			expect(numCalls())
				.to.eq(1);
		});

		it('returns true and notifies when all values are absent ', () => {
			const s = new ObservableSet<number>([ 5, 6, 7, 8 ]);

			const { observer, numCalls } = spyObserver();
			s.observe(observer);

			expect(s.addAll([ 9, 10, 11, 12 ]))
				.to.be.true;

			expect(s.toArray())
				.to.deep.eq([ 5, 6, 7, 8, 9, 10, 11, 12 ]);

			expect(numCalls())
				.to.eq(1);
		});
	});

	describe('delete()', () => {
		it('returns false & does not notify if value not present in set', () => {
			const s = new ObservableSet<number>([ 5 ]);

			const { observer, numCalls } = spyObserver();
			s.observe(observer);

			expect(s.delete(6))
				.to.be.false;

			expect(numCalls())
				.to.eq(0);
		});

		it('returns true & notifies if value was deleted from set', () => {
			const s = new ObservableSet<number>([ 5 ]);

			const { observer, numCalls } = spyObserver();
			s.observe(observer);

			expect(s.delete(5))
				.to.be.true;

			expect(numCalls())
				.to.eq(1);

			expect(s.has(5))
				.to.be.false;
		});
	});

	describe('deleteAll()', () => {
		it('returns false and does not notify when all values are already absent', () => {
			const s = new ObservableSet<number>([ 1, 2, 3, 4 ]);

			const { observer, numCalls } = spyObserver();
			s.observe(observer);

			expect(s.deleteAll([ 5, 6, 7, 8 ]))
				.to.be.false;

			expect(s.toArray())
				.to.deep.eq([ 1, 2, 3, 4 ]);

			expect(numCalls())
				.to.eq(0);
		});

		it('returns true and notifies when some values are absent ', () => {
			const s = new ObservableSet<number>([ 5, 6, 7, 8 ]);

			const { observer, numCalls } = spyObserver();
			s.observe(observer);

			expect(s.deleteAll([ 7, 8, 9, 10 ]))
				.to.be.true;

			expect(s.toArray())
				.to.deep.eq([ 5, 6 ]);

			expect(numCalls())
				.to.eq(1);
		});

		it('returns true and notifies when all values are present ', () => {
			const s = new ObservableSet<number>([ 5, 6, 7, 8 ]);

			const { observer, numCalls } = spyObserver();
			s.observe(observer);

			expect(s.deleteAll([ 5, 6, 7, 8 ]))
				.to.be.true;

			expect(s.toArray())
				.to.deep.eq([]);

			expect(numCalls())
				.to.eq(1);
		});
	});

	describe('clear()', () => {
		it('returns false & does not notify for already empty set', () => {
			const s = new ObservableSet<number>();

			const { observer, numCalls } = spyObserver();
			s.observe(observer);

			expect(s.clear())
				.to.be.false;

			expect(numCalls())
				.to.eq(0);
		});

		it('returns true & notifies for non-empty set', () => {
			const s = new ObservableSet<number>([ 5 ]);

			const { observer, numCalls } = spyObserver();
			s.observe(observer);

			expect(s.clear())
				.to.be.true;

			expect(numCalls())
				.to.eq(1);
		});
	});

	describe('replace()', () => {
		it('replaces all values in the set & notifies', () => {
			const s = new ObservableSet([ 5, 6, 7, 8 ]);
			const { observer, numCalls } = spyObserver();
			s.observe(observer);

			s.replace([ 1, 2, 3, 4 ]);

			expect(s.toArray())
				.to.deep.eq([ 1, 2, 3, 4 ]);

			expect(numCalls())
				.to.eq(1);
		});
	});

	describe('forEach()', () => {
		it('invokes observer for each item in this set', () => {
			const s = new ObservableSet<number>([ 5, 6, 7, 8 ]);

			const values: number[] = [];
			s.forEach((value, set) => {
				expect(set)
					.to.eq(s);

				values.push(value);
			});

			expect(values)
				.to.deep.eq([ 5, 6, 7, 8 ]);
		});
	});

	describe('filter()', () => {
		it('returns array of filtered values for type guard predicate', () => {
			const s = new ObservableSet([ 5, null, 6, null, 7, null, 8, null ]);

			const notNull = (x: number | null): x is number => x !== null;

			const f = s.filter(notNull);
			expect(f)
				.to.deep.eq([ 5, 6, 7, 8 ]);
		});

		it('returns array of filtered values for boolean predicate', () => {
			const s = new ObservableSet([ 5, null, 6, null, 7, null, 8, null ]);

			const notNull = (x: number | null) => x !== null;

			const f = s.filter(notNull);
			expect(f)
				.to.deep.eq([ 5, 6, 7, 8 ]);
		});
	});

	describe('map()', () => {
		it('returns array of mapped values', () => {
			const s = new ObservableSet<number>([ 5, 6, 7, 8 ]);

			let expectedIndex = 0;
			const result = s.map((value, index, set) => {
				expect(index)
					.to.eq(expectedIndex);
				++expectedIndex;

				expect(set)
					.to.eq(s);

				return (value + 10).toString();
			});

			expect(result)
				.to.deep.eq([ '15', '16', '17', '18' ]);
		});
	});

	describe('has()', () => {
		it('returns false for absent value', () => {
			const s = new ObservableSet<number>([ 5, 6, 7, 8 ]);

			expect(s.has(1))
				.to.be.false;
		});

		it('returns true for present value', () => {
			const s = new ObservableSet<number>([ 5, 6, 7, 8 ]);

			expect(s.has(5))
				.to.be.true;
		});
	});

	describe('size', () => {
		it('returns size of set', () => {
			const s = new ObservableSet<number>([ 5, 6, 7, 8 ]);

			expect(s.size)
				.to.eq(4);
		});
	});

	describe('toArray()', () => {
		it('returns array of all values in the set', () => {
			const s = new ObservableSet<number>([ 5, 6, 7, 8 ]);

			expect(s.toArray())
				.to.deep.eq([ 5, 6, 7, 8 ]);
		});
	});

	describe('iteration', () => {
		it('is iterable', () => {
			const s = new ObservableSet<number>([ 5, 6, 7, 8 ]);

			const values: number[] = [];
			for(const value of s) {
				values.push(value);
			}

			expect(values)
				.to.deep.eq([ 5, 6, 7, 8 ]);
		});
	});

	describe('toStringTag', () => {
		it('returns ObservableSet', () => {
			expect(new ObservableSet<number>()[Symbol.toStringTag])
				.to.eq('ObservableSet');
		});
	});

	describe('unobserve()', () => {
		it('removes observers', () => {
			const s = new ObservableSet<number>();

			const { observer, numCalls } = spyObserver();
			s.observe(observer);

			s.add(1);

			expect(numCalls())
				.to.eq(1);

			s.unobserve(observer);

			s.add(2);

			expect(numCalls())
				.to.eq(1);
		});
	});

	let refVal = 0;
	testRef(() => new ObservableSet<number>, s => s.add(refVal++));
});