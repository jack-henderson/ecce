import type { ReadonlyObservableMap } from '../../src';
import { describe, expect, it } from 'vitest';
import { ObservableMap } from '../../src';
import { spyObserver } from '../helpers';
import { testRef } from './test-ref';


describe('[ecce] ObservableMap', () => {
	describe('set()', () => {
		it('sets key/value & notifies observers', () => {
			const m = new ObservableMap<string, string>();

			const { observer, numCalls } = spyObserver();
			m.observe(observer);

			m.set('foo', 'bar');

			expect(m.get('foo'))
				.to.eq('bar');

			expect(numCalls())
				.to.eq(1);
		});
	});

	describe('setAll()', () => {
		it('sets all key/values & notifies', () => {
			const m = new ObservableMap<string, string>();

			const { observer, numCalls } = spyObserver();
			m.observe(observer);

			m.setAll([ ['foo', 'bar'], [ 'baz', 'qux' ] ]);

			expect(m.get('foo'))
				.to.eq('bar');

			expect(m.get('baz'))
				.to.eq('qux');

			expect(numCalls())
				.to.eq(1);
		});
	});

	describe('delete()', () => {
		it('returns false and does not notify if key was absent', () => {
			const m = new ObservableMap<string, string>();

			const { observer, numCalls } = spyObserver();
			m.observe(observer);

			expect(m.delete('foo'))
				.to.be.false;

			expect(numCalls())
				.to.eq(0);
		});

		describe('deleteAll()', () => {
			it('returns false and does not notify if the map is empty', () => {
				const m = new ObservableMap<string, string>();

				const { observer, numCalls } = spyObserver();
				m.observe(observer);

				expect(m.deleteAll(['baz', 'another']))
					.to.be.false;

				expect(numCalls())
					.to.eq(0);
			});

			it('returns false and does not notify if all keys were absent', () => {
				const m = new ObservableMap<string, string>([ [ 'foo', 'bar' ]]);

				const { observer, numCalls } = spyObserver();
				m.observe(observer);

				expect(m.deleteAll(['baz', 'another']))
					.to.be.false;

				expect(numCalls())
					.to.eq(0);
			});

			it('returns false and does not notify if no keys passed', () => {
				const m = new ObservableMap<string, string>([ [ 'foo', 'bar' ]]);

				const { observer, numCalls } = spyObserver();
				m.observe(observer);

				expect(m.deleteAll([]))
					.to.be.false;

				expect(numCalls())
					.to.eq(0);
			});

			it('returns true, removes entries, and notifies if some keys were present', () => {
				const m = new ObservableMap<string, string>([ [ 'foo', 'bar' ] , [ 'baz', 'qux' ]]);

				const { observer, numCalls } = spyObserver();
				m.observe(observer);

				expect(m.deleteAll([ 'baz', 'another' ]))
					.to.be.true;

				expect(m.has('baz'))
					.to.be.false;

				expect(numCalls())
					.to.eq(1);
			});

			it('returns true, removes entries, and notifies if all keys were present', () => {
				const m = new ObservableMap<string, string>([ [ 'foo', 'bar' ] , [ 'baz', 'qux' ]]);

				const { observer, numCalls } = spyObserver();
				m.observe(observer);

				expect(m.deleteAll([ 'baz', 'foo' ]))
					.to.be.true;

				expect(m.has('has'))
					.to.be.false;

				expect(m.has('baz'))
					.to.be.false;

				expect(numCalls())
					.to.eq(1);
			});
		});

		it('returns true, removes mapping, and notifies if key was present', () => {
			const m = new ObservableMap<string, string>([[ 'foo', 'bar' ]]);

			const { observer, numCalls } = spyObserver();
			m.observe(observer);

			expect(m.delete('foo'))
				.to.be.true;

			expect(m.get('foo'))
				.to.be.undefined;

			expect(numCalls())
				.to.eq(1);
		});
	});

	describe('clear()', () => {
		it('returns false and does not notify if the map is empty', () => {
			const m = new ObservableMap<string, string>();

			const { observer, numCalls } = spyObserver();
			m.observe(observer);

			expect(m.clear())
				.to.be.false;

			expect(numCalls())
				.to.eq(0);
		});

		it('returns true, clears the map, and notifies if key was present', () => {
			const m = new ObservableMap<string, string>([[ 'foo', 'bar' ]]);

			const { observer, numCalls } = spyObserver();
			m.observe(observer);

			expect(m.clear())
				.to.be.true;

			expect(m.get('foo'))
				.to.be.undefined;

			expect(m.size)
				.to.eq(0);

			expect(numCalls())
				.to.eq(1);
		});
	});

	describe('replace()', () => {
		it('does not notify if the map and new entries are both empty', () => {
			const m = new ObservableMap<string, string>();

			const { observer, numCalls } = spyObserver();
			m.observe(observer);

			m.replace([]);

			expect(numCalls())
				.to.eq(0);
		});

		it('replaces all entries and notifies with new entries', () => {
			const m = new ObservableMap<string, string>([ [ 'foo', 'bar' ], [ 'baz', 'qux' ] ]);

			const { observer, numCalls } = spyObserver();
			m.observe(observer);

			m.replace([ [ 'some', 'different' ], [ 'map', 'entries' ] ]);

			expect(m.has('foo'))
				.to.be.false;
			expect(m.has('bar'))
				.to.be.false;

			expect(m.get('some'))
				.to.eq('different');
			expect(m.get('map'))
				.to.eq('entries');


			expect(numCalls())
				.to.eq(1);
		});

		it('replaces all entries and notifies with empty entries', () => {
			const m = new ObservableMap<string, string>([ [ 'foo', 'bar' ], [ 'baz', 'qux' ] ]);

			const { observer, numCalls } = spyObserver();
			m.observe(observer);

			m.replace([]);

			expect(m.has('foo'))
				.to.be.false;
			expect(m.has('bar'))
				.to.be.false;
			expect(m.size)
				.to.eq(0);

			expect(numCalls())
				.to.eq(1);
		});
	});

	describe('has()', () => {
		it('returns true if the map has the passed key', () => {
			const m = new ObservableMap<string, string>([[ 'foo', 'bar' ]]);
			expect(m.has('foo'))
				.to.be.true;
		});

		it('returns false if the map does not have the passed key', () => {
			const m = new ObservableMap<string, string>([[ 'foo', 'bar' ]]);
			expect(m.has('baz'))
				.to.be.false;
		});
	});

	describe('iteration', () => {
		it('is iterable', () => {
			const m = new ObservableMap<string, string>([[ 'foo', 'bar' ], [ 'baz', 'qux' ]]);

			const entries: [ string, string ][] = [];
			for(const entry of m) {
				entries.push(entry);
			}

			expect(entries)
				.to.deep.eq([ [ 'foo', 'bar' ], [ 'baz', 'qux' ] ]);
		});
	});

	describe('entries()', () => {
		it('returns iterable of the map entries', () => {
			const obsMap = new ObservableMap([[ 'foo', 'bar' ], [ 'baz', 'qux' ]]);
			const map = new Map([[ 'foo', 'bar' ], [ 'baz', 'qux' ]]);

			expect([ ...obsMap.entries() ])
				.to.deep.eq([ ...map.entries() ]);
		});
	});

	describe('entryArray()', () => {
		it('returns array of the map entries', () => {
			const m = new ObservableMap([[ 'foo', 'bar' ], [ 'baz', 'qux' ]]);
			expect(m.entryArray())
				.to.deep.eq([ [ 'foo', 'bar' ], [ 'baz', 'qux' ] ]);
		});
	});

	describe('keys()', () => {
		it('returns iterable of the map keys', () => {
			const obsMap = new ObservableMap([[ 'foo', 'bar' ], [ 'baz', 'qux' ]]);
			const map = new Map([[ 'foo', 'bar' ], [ 'baz', 'qux' ]]);

			expect([ ...obsMap.keys() ])
				.to.deep.eq([ ...map.keys() ]);
		});
	});

	describe('keyArray()', () => {
		it('returns array of the map keys', () => {
			const m = new ObservableMap([[ 'foo', 'bar' ], [ 'baz', 'qux' ]]);
			expect(m.keyArray())
				.to.deep.eq([ 'foo', 'baz' ]);
		});
	});

	describe('values()', () => {
		it('returns iterable of the map values', () => {
			const obsMap = new ObservableMap([[ 'foo', 'bar' ], [ 'baz', 'qux' ]]);
			const map = new Map([[ 'foo', 'bar' ], [ 'baz', 'qux' ]]);

			expect([ ...obsMap.values() ])
				.to.deep.eq([ ...map.values() ]);
		});
	});

	describe('valueArray()', () => {
		it('returns array of the map values', () => {
			const m = new ObservableMap([[ 'foo', 'bar' ], [ 'baz', 'qux' ]]);
			expect(m.valueArray())
				.to.deep.eq([ 'bar', 'qux' ]);
		});
	});

	describe('forEach()', () => {
		it('invokes for each value/key pair in map', () => {
			const m = new ObservableMap<string, string>([[ 'foo', 'bar' ], [ 'baz', 'qux' ]]);

			const entries: [ string, string ][] = [];
			m.forEach((value, key, map) => {
				expect(map)
					.to.eq(m);

				entries.push([ key, value ]);
			});

			expect(entries)
				.to.deep.eq([ [ 'foo', 'bar' ], [ 'baz', 'qux' ] ]);
		});
	});

	describe('forEachEntry()', () => {
		it('invokes for each entry in map', () => {
			const m = new ObservableMap<string, string>([[ 'foo', 'bar' ], [ 'baz', 'qux' ]]);

			const entries: [ string, string ][] = [];
			m.forEachEntry((entry, map) => {
				expect(map)
					.to.eq(m);

				entries.push(entry);
			});

			expect(entries)
				.to.deep.eq([ [ 'foo', 'bar' ], [ 'baz', 'qux' ] ]);
		});

		describe('unobserve()', () => {
			it('removes observer', () => {
				const m = new ObservableMap<string, string>();

				const { observer, numCalls } = spyObserver();
				m.observe(observer);

				m.set('foo', 'bar');

				expect(numCalls())
					.to.eq(1);

				m.unobserve(observer);

				m.set('foo', 'baz');

				expect(numCalls())
					.to.eq(1);
			});
		});
	});

	let refKey = 0;
	testRef(() => new ObservableMap<number, number>, m => m.set(refKey++, 5));

	describe('ReadonlyObservableMap', () => {
		it('is assignable from ObservableMap', () => {
			const m: ReadonlyObservableMap<string, string> = new ObservableMap();

			// @ts-expect-error Should not be able to set().
			m.set('foo', 'bar');

			// @ts-expect-error Should not be able to delete().
			m.delete('foo');

			// @ts-expect-error Should not be able to clear().
			m.clear();
		});
	});
});