import { describe, expect, it } from 'vitest';
import { ObservableArray } from '../../src';
import { spyObserver } from '../helpers';
import { testRef } from './test-ref';


describe('[ecce] ObservableArray', () => {
	describe('constructor()', () => {
		it('creates an array of length', () => {
			expect(new ObservableArray<string>(5))
				.to.have.length(5);
		});
	});

	describe('at()', () => {
		it('returns undefined for out of bounds', () => {
			const a = new ObservableArray<string>([ 'foo', 'bar', 'baz', 'qux' ]);
			expect(a.at(10))
				.to.be.undefined;
		});

		it('returns element at positive index', () => {
			const a = new ObservableArray<string>([ 'foo', 'bar', 'baz', 'qux' ]);
			expect(a.at(1))
				.to.eq('bar');
		});

		it('returns element from end for negative index', () => {
			const a = new ObservableArray<string>([ 'foo', 'bar', 'baz', 'qux' ]);

			expect(a.at(-1))
				.to.eq('qux');
		});
	});

	describe('push()', () => {
		it('appends an element & notifies observers', () => {
			const a = new ObservableArray<string>([ 'foo', 'bar' ]);

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			a.push('baz');

			expect(a.toArray())
				.to.deep.eq([ 'foo', 'bar', 'baz' ]);

			expect(numCalls())
				.to.eq(1);
		});
	});

	describe('pushAll()', () => {
		it('appends elements & notifies observers', () => {
			const a = new ObservableArray<string>([ 'foo' ]);

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			a.pushAll(['bar', 'baz' ]);

			expect(a.toArray())
				.to.deep.eq([ 'foo', 'bar', 'baz' ]);

			expect(numCalls())
				.to.eq(1);
		});

		it('does nothing & does not notify observers for empty iterable', () => {
			const a = new ObservableArray<string>([ 'foo', 'bar' ]);

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			a.pushAll([]);

			expect(a.toArray())
				.to.deep.eq([ 'foo', 'bar' ]);


			expect(numCalls())
				.to.eq(0);
		});
	});

	describe('pop()', () => {
		it('removes & results the last element & notifies observers', () => {
			const a = new ObservableArray<string>([ 'foo', 'bar' ]);

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			expect(a.pop())
				.to.eq('bar');

			expect(a.toArray())
				.to.deep.eq([ 'foo' ]);

			expect(numCalls())
				.to.eq(1);
		});

		it('returns undefined & does not notify for empty array', () => {
			const a = new ObservableArray<string>();

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			expect(a.pop())
				.to.be.undefined;

			expect(numCalls())
				.to.eq(0);
		});
	});

	describe('unshift()', () => {
		it('prepends an element & notifies observers', () => {
			const a = new ObservableArray<string>([ 'bar', 'baz' ]);

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			a.unshift('foo');

			expect(a.toArray())
				.to.deep.eq([ 'foo', 'bar', 'baz' ]);

			expect(numCalls())
				.to.eq(1);
		});
	});

	describe('unshiftAll()', () => {
		it('appends elements & notifies observers', () => {
			const a = new ObservableArray<string>([ 'baz' ]);

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			a.unshiftAll([ 'foo', 'bar' ]);

			expect(a.toArray())
				.to.deep.eq([ 'foo', 'bar', 'baz' ]);

			expect(numCalls())
				.to.eq(1);
		});

		it('does nothing & does not notify observers for empty iterable', () => {
			const a = new ObservableArray<string>([ 'foo', 'bar' ]);

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			a.unshiftAll([]);

			expect(a.toArray())
				.to.deep.eq([ 'foo', 'bar' ]);

			expect(numCalls())
				.to.eq(0);
		});
	});

	describe('shift()', () => {
		it('removes & results the first element & notifies observers', () => {
			const a = new ObservableArray<string>([ 'foo', 'bar' ]);

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			expect(a.shift())
				.to.eq('foo');

			expect(a.toArray())
				.to.deep.eq([ 'bar' ]);

			expect(numCalls())
				.to.eq(1);
		});

		it('returns undefined & does not notify for empty array', () => {
			const a = new ObservableArray<string>();

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			expect(a.shift())
				.to.be.undefined;

			expect(numCalls())
				.to.eq(0);
		});
	});

	describe('sort()', () => {
		it('sorts array in place & notifies observers', () => {
			const a = new ObservableArray<string>([ 'c', 'd', 'b', 'e', 'a' ]);

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			a.sort();

			expect(a.toArray())
				.to.deep.eq([ 'a', 'b', 'c', 'd', 'e' ]);

			expect(numCalls())
				.to.eq(1);
		});


		it('does nothing & does not notify observers for empty array', () => {
			const a = new ObservableArray<string>();

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			a.sort();

			expect(numCalls())
				.to.eq(0);
		});
	});

	describe('reverse()', () => {
		it('reverses array & notifies', () => {
			const a = new ObservableArray<string>([ 'e', 'd', 'c', 'b', 'a' ]);

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			a.reverse();

			expect(a.toArray())
				.to.deep.eq([ 'a', 'b', 'c', 'd', 'e' ]);

			expect(numCalls())
				.to.eq(1);
		});

		it('does nothing and does not notify for empty array', () => {
			const a = new ObservableArray<string>([]);

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			a.reverse();

			expect(a.toArray())
				.to.deep.eq([]);

			expect(numCalls())
				.to.eq(0);
		});
	});

	describe('splice()', () => {
		it('inserts elements at index & notifies', () => {
			const a = new ObservableArray<string>(['foo', 'qux']);

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			a.splice(1, 0, 'bar', 'baz');

			expect(a.toArray())
				.to.deep.eq([ 'foo', 'bar', 'baz', 'qux' ]);

			expect(numCalls())
				.to.eq(1);
		});

		it('removes elements at index & notifies', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			a.splice(1, 2);

			expect(a.toArray())
				.to.deep.eq([ 'foo', 'qux' ]);

			expect(numCalls())
				.to.eq(1);
		});
	});

	describe('length', () => {
		describe('set', () => {
			it('updates length & notifies observers for new length', () => {
				const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

				const { observer, numCalls } = spyObserver();
				a.observe(observer);

				a.length = 2;

				expect(a.toArray())
					.to.deep.eq([ 'foo', 'bar' ]);

				expect(numCalls())
					.to.eq(1);
			});

			it('does nothing & does not notify observers for new length', () => {
				const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

				const { observer, numCalls } = spyObserver();
				a.observe(observer);

				a.length = 4;

				expect(a.toArray())
					.to.deep.eq(['foo', 'bar', 'baz', 'qux']);

				expect(numCalls())
					.to.eq(0);
			});
		});
	});

	describe('clear()', () => {
		it('removes all elements & notifies observers', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			a.clear();
			expect(a.toArray())
				.to.deep.eq([]);

			expect(numCalls())
				.to.eq(1);
		});

		it('does nothing & does not notify observers for empty array', () => {
			const a = new ObservableArray<string>([]);

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			expect(a.toArray())
				.to.deep.eq([]);

			a.clear();

			expect(numCalls())
				.to.eq(0);
		});
	});

	describe('replaces()', () => {
		it('replaces all elements & notifies observers', () => {
			const a = new ObservableArray<string>(['foo', 'bar']);

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			a.replace(['baz', 'qux']);
			expect(a.toArray())
				.to.deep.eq(['baz', 'qux']);


			expect(numCalls())
				.to.eq(1);
		});
	});

	describe('find()', () => {
		it('returns first element to match predicate', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			let expectedIndex = 0;
			const result = a.find((el, index, array) => {
				expect(array)
					.to.eq(a);
				expect(index)
					.to.eq(expectedIndex++);

				return el.startsWith('b');
			});


			expect(result)
				.to.eq('bar');
		});

		it('returns undefined if no elements match', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			expect(a.find(el => el.startsWith('z')))
				.to.be.undefined;
		});
	});

	describe('findIndex()', () => {
		it('returns the index of the first element to match predicate', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			let expectedIndex = 0;
			const result = a.findIndex((el, index, array) => {
				expect(array)
					.to.eq(a);
				expect(index)
					.to.eq(expectedIndex++);

				return el.startsWith('b');
			});


			expect(result)
				.to.eq(1);
		});

		it('returns -1 if no elements match', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			expect(a.findIndex(el => el.startsWith('z')))
				.to.eq(-1);
		});
	});

	describe('findLast()', () => {
		it('returns the last element to match predicate', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			let expectedIndex = a.length - 1;
			const result = a.findLast((el, index, array) => {
				expect(array)
					.to.eq(a);
				expect(index)
					.to.eq(expectedIndex--);

				return el.startsWith('b');
			});


			expect(result)
				.to.eq('baz');
		});

		it('returns undefined if no elements match', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			expect(a.findLast(el => el.startsWith('z')))
				.to.be.undefined;
		});
	});

	describe('findLastIndex()', () => {
		it('returns the index of the last element to match predicate', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			let expectedIndex = a.length - 1;
			const result = a.findLastIndex((el, index, array) => {
				expect(array)
					.to.eq(a);
				expect(index)
					.to.eq(expectedIndex--);

				return el.startsWith('b');
			});


			expect(result)
				.to.eq(2);
		});

		it('returns -1 if no elements match', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			expect(a.findLastIndex(el => el.startsWith('z')))
				.to.eq(-1);
		});
	});

	describe('includes()', () => {
		it('returns true if element is present in array', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			expect(a.includes('baz'))
				.to.be.true;
		});

		it('returns false if element is not present in array', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'qux']);

			expect(a.includes('baz'))
				.to.be.false;
		});

		it('returns false if element is not present in array after fromIndex', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			expect(a.includes('foo', 2))
				.to.be.false;
		});
	});

	describe('some()', () => {
		it('returns true if at least one element matches predicate', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			let expectedIndex = 0;
			const result = a.some((el, index, array) => {
				expect(array)
					.to.eq(a);
				expect(index)
					.to.eq(expectedIndex++);

				return el.startsWith('b');
			});


			expect(result)
				.to.be.true;
		});

		it('returns false if no elements match predicate', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			expect(a.some(el => el.startsWith('z')))
				.to.be.false;
		});
	});

	describe('every()', () => {
		it('returns true if every element matches predicate', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			let expectedIndex = 0;
			const result = a.every((el, index, array) => {
				expect(array)
					.to.eq(a);
				expect(index)
					.to.eq(expectedIndex++);

				return el.length === 3;
			});


			expect(result)
				.to.be.true;
		});

		it('returns false if some elements do not match predicate', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			expect(a.every(el => el.startsWith('b')))
				.to.be.false;
		});
	});

	describe('iteration', () => {
		it('is iterable', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			const elements: string[] = [];
			for(const element of a) {
				elements.push(element);
			}

			expect(elements)
				.to.deep.eq(a.toArray());
		});
	});

	describe('entries()', () => {
		it('returns iterable of entries', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);
			for(const [ index, value ] of a.entries()) {
				expect(a.at(index))
					.to.eq(value);
			}
		});
	});

	describe('forEach()', () => {
		it('invokes observer for each element', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			let expectedIndex = 0;
			a.forEach((el, index, array) => {
				expect(array)
					.to.eq(a);
				expect(index)
					.to.eq(expectedIndex++);

				expect(el)
					.to.eq(a.at(index));
			});
		});
	});

	describe('filter()', () => {
		it('returns array of elements which match predicate', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			let expectedIndex = 0;
			const result = a.filter((el, index, array) => {
				expect(array)
					.to.eq(a);
				expect(index)
					.to.eq(expectedIndex++);

				return el.startsWith('b');
			});

			expect(result)
				.to.deep.eq([ 'bar', 'baz' ]);
		});

		it('returns empty array if no elements match', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			expect(a.filter(el => el.startsWith('z')))
				.to.be.empty;
		});
	});

	describe('flat()', () => {
		it('returns identical array for already flat array', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);

			expect(a.flat())
				.to.deep.eq(a.toArray());
		});

		it('returns flattened array for array of arrays', () => {
			const a = new ObservableArray<unknown>([ ['foo', 'bar' ], ['baz', ['qux'] ] ]);
			expect(a.flat(2))
				.to.deep.eq(['foo', 'bar', 'baz', 'qux']);
		});
	});

	describe('flatMap()', () => {
		it('returns flat array of mapped values', () => {
			const a = new ObservableArray<string>(['foo', 'bar' ]);

			const result = a.flatMap(e => e.split(''));
			expect(result)
				.to.deep.eq([
					'f', 'o', 'o', 'b', 'a', 'r',
				]);
		});
	});

	describe('map()', () => {
		it('returns array of mapped values', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);
			let expectedIndex = 0;
			const result = a.map((el, index, array) => {
				expect(array)
					.to.eq(a);
				expect(index)
					.to.eq(expectedIndex++);

				return el.toUpperCase();
			});

			expect(result)
				.to.deep.eq([ 'FOO', 'BAR', 'BAZ', 'QUX' ]);
		});
	});

	describe('reduce()', () => {
		it('reduces array', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);
			let expectedIndex = 0;
			const result = a.reduce((value: Record<string, unknown>, element, index, array) => {
				expect(array)
					.to.eq(a);
				expect(index)
					.to.eq(expectedIndex++);

				value[element] = index;
				return value;
			}, {});

			expect(result)
				.to.deep.eq({
					foo: 0,
					bar: 1,
					baz: 2,
					qux: 3,
				});
		});
	});

	describe('reduceRight()', () => {
		it('reduces array', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);
			let expectedIndex = a.length - 1;
			const result = a.reduceRight((value: Record<string, unknown>, element, index, array) => {
				expect(array)
					.to.eq(a);
				expect(index)
					.to.eq(expectedIndex--);

				value[element] = index;
				return value;
			}, {});

			expect(result)
				.to.deep.eq({
					qux: 3,
					baz: 2,
					bar: 1,
					foo: 0,
				});
		});
	});

	describe('slice()', () => {
		it('returns sliced array', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);
			expect(a.slice(1, -1))
				.to.deep.eq([ 'bar', 'baz' ]);
		});
	});

	describe('join()', () => {
		it('returns joined string', () => {
			const a = new ObservableArray<string>(['foo', 'bar', 'baz', 'qux']);
			expect(a.join(' & '))
				.to.deep.eq('foo & bar & baz & qux');
		});
	});

	describe('toString()', () => {
		it('returns string representation of items', () => {
			class El {
				constructor(private readonly value: string) {}

				toString() {
					return this.value;
				}
			}

			const a = new ObservableArray<El>([ new El('foo'),  new El('bar'),  new El('baz'),  new El('qux') ]);
			expect(a.toString())
				.to.deep.eq('foo,bar,baz,qux');
		});
	});

	describe('toLocaleString()', () => {
		it('returns locals string representation', () => {
			class El {
				constructor(private readonly value: string) {}

				toLocaleString() {
					return this.value;
				}
			}

			const a = new ObservableArray<El>([ new El('foo'),  new El('bar'),  new El('baz'),  new El('qux') ]);
			expect(a.toLocaleString())
				.to.deep.eq('foo,bar,baz,qux');
		});
	});

	describe('unobserve()', () => {
		it('removes an observer', () => {
			const a = new ObservableArray<string>(['foo', 'bar']);

			const { observer, numCalls } = spyObserver();
			a.observe(observer);

			a.push('baz');

			expect(numCalls())
				.to.eq(1);

			a.unobserve(observer);

			a.push('qux');

			expect(numCalls())
				.to.eq(1);
		});
	});

	let refVal = 0;
	testRef(() => new ObservableArray<number>(), a => a.push(refVal++));
});