import type { ObservableCollection } from '../../src/collections/observable-collection';
import { describe, expect, it } from 'vitest';


export const testRef = <T extends ObservableCollection>(makeCollection: () => T, mutateCollection: (collection: T) => void) => {
	describe('ref', () => {
		it('returns referentially equal object if unchanged', () => {
			const c = makeCollection();

			expect(c.ref)
				.to.eq(c.ref);
		});

		it('returns referentially unequal object after each change', () => {
			const c = makeCollection();
			const ref1 = c.ref;

			mutateCollection(c);

			const ref2 = c.ref;
			expect(ref1)
				.to.not.eq(ref2);

			mutateCollection(c);

			const ref3 = c.ref;
			expect(ref1)
				.to.not.eq(ref3);
			expect(ref2)
				.to.not.eq(ref3);
		});
	});
};