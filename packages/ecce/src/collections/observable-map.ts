
import { ObservableCollection } from './observable-collection';

/**
 * An observable map.
 *
 * Observers will be notified whenever the entries in the map are changed.
 */
export class ObservableMap<K, V> extends ObservableCollection implements Iterable<[K, V]> {
	private readonly map: Map<K, V>;

	constructor();
	constructor(entries?: readonly (readonly [key: K, value: V])[] | null);
	constructor(iterable?: Iterable<readonly [key: K, value: V]> | null | undefined);
	constructor(arg?: readonly (readonly [K, V])[] | Iterable<readonly [K, V]> | null | undefined) {
		super();
		this.map = new Map(arg);
	}

	/**
	 * Get the element associated with the passed `key`.
	 *
	 * @param key
	 * @returns the element associated with the passed `key`, or `undefined` if
	 * 	no such key exists.
	 */
	get(key: K): V | undefined {
		return this.map.get(key);
	}

	/**
	 * Set or update the element associated with `key`.
	 *
	 * Observers will be notified.
	 *
	 * @param key
	 * @param value
	 */
	set(key: K, value: V): void {
		this.map.set(key, value);
		this._changed();
	}

	/**
	 * Set or update multiple map entries.
	 *
	 * Observers will be notified.
	 */
	setAll(entries: Iterable<[ key: K, value: V ]>): void {
		for(const [ key, value ] of entries) {
			this.map.set(key, value);
		}
		this._changed();
	}

	/**
	 * Remove the element associated with `key`.
	 *
	 * If there was previously an element associated with `key`, observers will be
	 * notified.
	 *
	 * @param key
	 * @returns `true` if the element was removed, or `false` if there was no
	 *  element associated with `key`.
	 */
	delete(key: K): boolean {
		if(this.map.delete(key)) {
			this._changed();
			return true;
		}

		return false;
	}

	deleteAll(keys: Iterable<K>): boolean {
		if(this.map.size === 0) {
			return false;
		}

		let anyDeleted = false;
		for(const key of keys) {
			anyDeleted = this.map.delete(key) || anyDeleted;
		}

		if(anyDeleted) {
			this._changed();
		}

		return anyDeleted;
	}

	/**
	 * Remove all elements from the map.
	 *
	 * If the set previously had any elements, observers will be notified.
	 *
	 * @returns `true` if the map was cleared, or `false` if the map was already
	 * 	empty.
	 */
	clear(): boolean {
		if(this.map.size === 0) {
			return false;
		}

		this.map.clear();
		this._changed();

		return true;
	}

	/**
	 * Replace all key/value pairs in the map.
	 *
	 * If the map was not previously empty and entries is not empty, Observers
	 * will be notified.
	 *
	 * @param entries
	 */
	replace(entries: Iterable<[ key: K, value: V ]>): void {
		let didChange = false;
		if(this.map.size !== 0) {
			this.map.clear();
			didChange = true;
		}

		for(const [ key, value ] of entries) {
			this.map.set(key, value);
			didChange = true;
		}

		if(didChange) {
			this._changed();
		}
	}

	has(key: K): boolean {
		return this.map.has(key);
	}

	get size() {
		return this.map.size;
	}

	[Symbol.iterator](): Iterator<[key: K, value: V]> {
		return this.map[Symbol.iterator]();
	}

	/**
	 * @returns an iterable of this map's entries.
	 */
	entries(): IterableIterator<[key: K, value: V]> {
		return this.map.entries();
	}

	/**
	 * Create an array containing this map's entries, in insertion order.
	 */
	entryArray(): [key: K, value: V][] {
		return [ ...this.map.entries() ];
	}

	/**
	 * @returns an iterable of this map's keys.
	 */
	keys(): IterableIterator<K> {
		return this.map.keys();
	}

	/**
	 * Create an array containing this map's keys, in insertion order.
	 */
	keyArray(): K[] {
		return [ ...this.map.keys() ];
	}

	/**
	 * @returns an iterable of this map's values.
	 */
	values(): IterableIterator<V> {
		return this.map.values();
	}

	/**
	 * Create an array containing this map's values, in insertion order.
	 */
	valueArray(): V[] {
		return [ ...this.map.values() ];
	}

	/**
	 * Invokes `callback` for each value/key pair in the map, in insertion order.
	 */
	forEach(callback: (value: V, key: K, map: ObservableMap<K, V>) => void): void {
		this.map.forEach((value, key) => callback(value, key, this));
	}

	/**
	 * Invokes `callback` for each entry in the map, in insertion order.
	 */
	forEachEntry(callback: (entry: [key: K, value: V], map: ObservableMap<K, V>) => void): void {
		for(const entry of this.entries()) {
			callback(entry, this);
		}
	}

	readonly [Symbol.toStringTag]: string = 'ObservableMap';
}

export type ReadonlyObservableMap<K, V> = Pick<ObservableMap<K, V>, (
	| 'has'
	| 'size'
	| typeof Symbol.iterator
	| 'entries'
	| 'entryArray'
	| 'keys'
	| 'keyArray'
	| 'values'
	| 'valueArray'
	| 'forEach'
	| 'forEachEntry'
	| typeof Symbol.toStringTag
	| 'observe'
	| 'unobserve'
)>;