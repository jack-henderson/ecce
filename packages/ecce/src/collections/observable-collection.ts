import { Subject } from '../subject';


/**
 * Base class for an observable collection.
 */
export abstract class ObservableCollection {
	private readonly _subject: Subject;

	constructor() {
		this._subject = Subject.getOrCreate(this);
	}

	/**
	 * Register a callback to be invoked each time this collection is mutated.
	 */
	observe(callback: VoidFunction): void {
		this._subject.observe(callback);
	}

	/**
	 * Unregister a callback.
	 */
	unobserve(callback: VoidFunction): void {
		this._subject.unobserve(callback);
	}

	/**
	 * Invoke from sub-classes to notify observers & update `ref`.
	 *
	 * @private
	 */
	protected _changed() {
		this._subject.notify();
	}

	/**
	 * An arbitrary value which changes each time this collection is mutated.
	 *
	 * Useful for integrating with reactive environments which use referential
	 * equality to trigger updates, for example:
	 *
	 * ```js
	 *    useEffect(() => { ... }, [ collection.ref ])
	 * ```
	 */
	get ref(): unknown {
		return this._subject.ref;
	}
}