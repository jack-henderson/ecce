import { ObservableCollection } from './observable-collection';


/**
 * An observable set.
 *
 * Observers will be notified whenever the content of the set is changed.
 */
export class ObservableSet<T> extends ObservableCollection implements Iterable<T> {
	private set: Set<T>;

	constructor(iterable?: Iterable<T> | null | undefined) {
		super();
		this.set = new Set(iterable);
	}

	/**
	 * Add a single element to the set.
	 *
	 * If the element was not previously present in the set, observers will be
	 * notified.
	 *
	 * @param element the element to add to the set.
	 * @returns `true` if the element was added, or `false` if it was already
	 * 	present.
	 */
	add(element: T): boolean {
		const preSize = this.set.size;
		this.set.add(element);
		if(preSize !== this.set.size) {
			this._changed();
			return true;
		}

		return false;
	}

	/**
	 * Add multiple elements to the set.
	 *
	 * If any of the passed elements were not present in the set,
	 * observers will be notified.
	 *
	 * @param elements the elements to add to the set.
	 * @returns `true` if any of the elements were added, or `false` if all the elements
	 * 	were already present.
	 */
	addAll(elements: Iterable<T>): boolean {
		const preSize = this.set.size;
		for(const element of elements) {
			this.set.add(element);
		}

		if(preSize !== this.set.size) {
			this._changed();
			return true;
		}

		return false;
	}

	/**
	 * Remove a single element from the set.
	 *
	 * If the element was previously present in the set, observers will be notified.
	 *
	 * @param element the element to remove.
	 * @returns `true` if the element was removed, or `false` if the element was not
	 * 	present in the set.
	 */
	delete(element: T): boolean {
		if(this.set.delete(element)) {
			this._changed();
			return true;
		}

		return false;
	}

	/**
	 * Remove multiple elements from the set.
	 *
	 * If any elements were previously present in the set, observers will be
	 * notified.
	 *
	 * @param elements the elements to remove.
	 * @returns `true` if any of the elements were removed, or `false` if none of
	 * 	the elements was present in the set.
	 */
	deleteAll(elements: Iterable<T>): boolean {
		let anyRemoved = false;
		for(const element of elements) {
			anyRemoved = this.set.delete(element) || anyRemoved;
		}

		if(anyRemoved) {
			this._changed();
		}

		return anyRemoved;
	}

	/**
	 * Remove all elements from the set.
	 *
	 * If the set previously had any elements, observers will be notified.
	 *
	 * @returns `true` if the set was cleared, or `false` if the set was already
	 * 	empty.
	 */
	clear(): boolean {
		if(this.set.size === 0) {
			return false;
		}

		this.set.clear();
		this._changed();
		return true;
	}

	/**
	 * Replace all elements in the set.
	 */
	replace(elements: Iterable<T>): void {
		this.set.clear();
		for(const element of elements) {
			this.set.add(element);
		}

		this._changed();
	}

	/**
	 * Invokes `callback` for each element in the set, in insertion order.
	 */
	forEach(callback: (value: T, set: ObservableSet<T>) => void): void {
		this.set.forEach(value => callback(value, this));
	}

	/**
	 * Create an array of this set's elements for which `predicate` returns true.
	 */
	filter<S extends T>(predicate: (value: T, set: ObservableSet<T>) => value is S): S[];
	filter(predicate: (value: T, set: ObservableSet<T>) => unknown): T[];
	filter(predicate: (value: T, set: ObservableSet<T>) => unknown): any[] {
		const filteredElements: T[] = [];

		for(const element of this.set) {
			if(predicate(element, this)) {
				filteredElements.push(element);
			}
		}

		return filteredElements;
	}

	/**
	 * Create an array of this set's elements, mapped by `callback`.
	 */
	map<U>(callback: (value: T, index: number, set: ObservableSet<T>) => U): U[] {
		const mappedElements: U[] = [];
		for(const element of this.set) {
			const mappedValue = callback(element, mappedElements.length, this);
			mappedElements.push(mappedValue);
		}

		return mappedElements;
	}

	/**
	 * Test if `element` is present in the set.
	 */
	has(element: T): boolean {
		return this.set.has(element);
	}

	/**
	 * Get the number of elements in the set.
	 */
	get size(): number {
		return this.set.size;
	}

	/**
	 * Create an array containing the elements of this set, in insertion order.
	 */
	toArray(): T[] {
		return [ ...this.set ];
	}

	[Symbol.iterator](): IterableIterator<T> {
		return this.set[Symbol.iterator]();
	}

	readonly [Symbol.toStringTag] = 'ObservableSet';
}

export type ReadonlyObservableSet<T> = Pick<ObservableSet<T>, (
	| 'forEach'
	| 'filter'
	| 'map'
	| 'has'
	| 'size'
	| 'toArray'
	| typeof Symbol.iterator
	| typeof Symbol.toStringTag
	| 'observe'
	| 'unobserve'
)>;