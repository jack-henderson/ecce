import { ObservableCollection } from './observable-collection';


type ObservableArrayCallback<T, R = void> = (element: T, index: number, array: ObservableArray<T>) => R;
type ObservableArrayReducer<T, R> = (previousElement: R, currentElement: T, index: number, array: ObservableArray<T>) => R;

export class ObservableArray<T> extends ObservableCollection implements Iterable<T> {
	private readonly _array: T[];

	constructor();
	constructor(length: number);
	constructor(items: Iterable<T>);
	constructor(itemsOrLength?: number | Iterable<T>) {
		super();
		if(typeof(itemsOrLength) === 'number') {
			this._array = new Array<T>(itemsOrLength);
		} else if(itemsOrLength) {
			this._array = Array.from(itemsOrLength);
		} else {
			this._array = [];
		}
	}

	at(index: number): T | undefined {
		return this._array[index < 0 ? this._array.length + index : index];
	}

	push(element: T): void {
		this._array.push(element);
		this._changed();
	}

	pushAll(elements: Iterable<T>): void {
		const preLength = this._array.length;
		this._array.push(...elements);

		if(preLength !== this._array.length) {
			this._changed();
		}
	}

	pop(): T | undefined {
		if(this._array.length === 0) {
			return undefined;
		}

		const element = this._array.pop();
		this._changed();

		return element;
	}

	unshift(element: T): void {
		this._array.unshift(element);
		this._changed();
	}

	unshiftAll(elements: Iterable<T>): void {
		const preLength = this._array.length;
		this._array.unshift(...elements);

		if(preLength !== this._array.length) {
			this._changed();
		}
	}

	shift(): T | undefined {
		if(this._array.length === 0) {
			return undefined;
		}

		const element = this._array.shift();
		this._changed();

		return element;
	}

	sort(): void {
		if(!this._array.length) {
			return;
		}

		this._array.sort();
		this._changed();
	}

	reverse(): void {
		if(!this._array.length) {
			return;
		}

		this._array.reverse();
		this._changed();
	}

	splice(start: number): void;
	splice(start: number, deleteCount: number): void;
	splice(start: number, deleteCount: number, ...elements: T[]): void;
	splice(start: number, deleteCount?: any, ...elements: T[]): void {
		this._array.splice(start, deleteCount, ...elements);
		this._changed();
	}

	get length(): number {
		return this._array.length;
	}

	set length(nextLength: number) {
		if(this._array.length === nextLength) {
			return;
		}

		this._array.length = nextLength;
		this._changed();
	}

	/**
	 * Remove all elements from the array.
	 */
	clear(): void {
		if(!this._array.length) {
			return;
		}

		this.length = 0;
	}

	/**
	 * Replace all elements in the array.
	 * @param elements
	 */
	replace(elements: Iterable<T>) {
		this._array.length = 0;
		this._array.push(...elements);
		this._changed();
	}

	includes(el: T, fromIndex?: number): boolean {
		return this._array.includes(el, fromIndex);
	}

	some(predicate: ObservableArrayCallback<T, boolean>): boolean {
		return this._array.some((element, index) => predicate(element, index, this));
	}

	every(predicate: ObservableArrayCallback<T, boolean>): boolean {
		return this._array.every((element, index) => predicate(element, index, this));
	}

	find(predicate: ObservableArrayCallback<T, boolean>): T | undefined {
		return this._array.find((element, index) => predicate(element, index, this));
	}

	findIndex(predicate: ObservableArrayCallback<T, boolean>): number {
		return this._array.findIndex((element, index) => predicate(element, index, this));
	}

	findLast(predicate: ObservableArrayCallback<T, boolean>): T | undefined {
		for(let i=this._array.length - 1; i>=0; --i) {
			const element = this._array[i]!; // eslint-disable-line @typescript-eslint/no-non-null-assertion
			if(predicate(element, i, this)) {
				return element;
			}
		}

		return undefined;
	}

	findLastIndex(predicate: ObservableArrayCallback<T, boolean>): number {
		for(let i=this._array.length - 1; i>=0; --i) {
			const element = this._array[i]!; // eslint-disable-line @typescript-eslint/no-non-null-assertion
			if(predicate(element, i, this)) {
				return i;
			}
		}

		return -1;
	}

	[Symbol.iterator](): Iterator<T, any, undefined> {
		return this._array[Symbol.iterator]();
	}

	entries(): IterableIterator<[index: number, element: T]> {
		return this._array.entries();
	}

	forEach(callback: (element: T, index: number, array: ObservableArray<T>) => void): void {
		this._array.forEach((element, index) => callback(element, index, this));
	}

	filter(predicate: ObservableArrayCallback<T, boolean>): T[] {
		return this._array.filter((element, index) => predicate(element, index, this));
	}

	flat<D extends number = 1>(depth?: D): FlatArray<T[], D>[] {
		return this._array.flat(depth);
	}

	flatMap<U>(fn: ObservableArrayCallback<T, U | ReadonlyArray<U>>): U[] {
		return this._array.flatMap((value, index) => fn(value, index, this));
	}

	map<U>(fn: ObservableArrayCallback<T, U>): U[] {
		return this._array.map((value, index) => fn(value, index, this));
	}

	reduce(fn: ObservableArrayReducer<T, T>): T;
	reduce(fn: ObservableArrayReducer<T, T>, initialValue: T): T;
	reduce<U>(fn: ObservableArrayReducer<T, U>, initialValue: U): U;
	reduce(fn: ObservableArrayReducer<any, any>, initialValue?: any): any {
		return this._array.reduce((previousValue, currentValue, index) => (
			fn(previousValue, currentValue, index, this)
		), initialValue);
	}

	reduceRight(fn: ObservableArrayReducer<T, T>): T;
	reduceRight(fn: ObservableArrayReducer<T, T>, initialValue: T): T;
	reduceRight<U>(fn: ObservableArrayReducer<T, U>, initialValue: U): U;
	reduceRight(fn: ObservableArrayReducer<any, any>, initialValue?: any): any {
		return this._array.reduceRight((previousValue, currentValue, index) => (
			fn(previousValue, currentValue, index, this)
		), initialValue);
	}

	slice(start?: number, end?: number): T[] {
		return this._array.slice(start, end);
	}

	join(separator?: string): string {
		return this._array.join(separator);
	}

	override toString(): string {
		return this._array.toString();
	}

	override toLocaleString(): string {
		return this._array.toLocaleString();
	}

	toArray(): T[] {
		return this._array.slice(0);
	}
}

export type ReadonlyObservableArray<T> = Pick<Readonly<ObservableArray<T>>, (
	| 'at'
	| 'length'
	| 'includes'
	| 'some'
	| 'every'
	| 'find'
	| 'findIndex'
	| 'findLast'
	| 'findLastIndex'
	| typeof Symbol.iterator
	| 'entries'
	| 'forEach'
	| 'filter'
	| 'flat'
	| 'flatMap'
	| 'map'
	| 'reduce'
	| 'reduceRight'
	| 'slice'
	| 'join'
	| 'toString'
	| 'toLocaleString'
	| 'toArray'
	| 'observe'
	| 'unobserve'
)>;