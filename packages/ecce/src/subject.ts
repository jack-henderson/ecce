import type { Observer } from './observe';
import { EcceError } from './error';
import { ObserverSet } from './observer-set';
import { isClassInstance, typeName } from './util';


/**
 * Manages observable behaviour for a single object or property.
 *
 * @access private
 */
export class Subject {
	private static readonly _instances = new WeakMap<object, Subject>();

	readonly name: string;
	private _observers: ObserverSet | null = null;
	private _owner: Subject | null;
	private _properties: Map<PropertyKey, Subject> | null = null;

	private _ref: unknown = null;

	private constructor(name: string, owner: Subject | null) {
		this.name = name;
		this._owner = owner;
	}

	/**
	 * Get the Subject instance for `target`, or create a new one if `target` has
	 * no Subject.
	 */
	static getOrCreate(target: any, name?: string): Subject {
		let subject = Subject._instances.get(target);
		if(!subject) {
			subject = new Subject(name ?? typeName(target), null);
			Subject._instances.set(target, subject);
		}

		return subject;
	}

	/**
	 * Get the Subject instance for `target`, or null if `target` has no Subject.
	 */
	static get(target: any): Subject | null {
		return Subject._instances.get(target) ?? null;
	}

	/**
	 * Find the Subject instance for `target`, or for a specific `key` of `target`.
	 *
	 * Enforces that `target` & `key` are valid targets for observation.
	 *
	 * @param target The object to find the Subject for.
	 * @param key Optionally, find the Subject for a specific member of `target`.
	 * @param functionName The name of the function this method is called from,
	 * 	used to provide helpful error messages.
	 *
	 * @throws If `target` is not an object.
	 * @throws If `target` is not observable.
	 * @throws If `key` is not an observable property of `target`
	 */
	static find(target: any, key: PropertyKey | undefined | null, functionName: string): Subject {
		if(target === null || target === undefined) {
			throw new TypeError(`Invalid target for ${functionName}(): target is ${target}.`);
		}
		if(typeof(target) !== 'object') {
			throw new TypeError(`Invalid target for ${functionName}(): \`${target}\` is not an object.`);
		}

		const rootSubject = Subject.get(target);
		if(!rootSubject) {
			if(isClassInstance(target)) {
				throw new EcceError(`Cannot ${functionName}() a class with no @observable() decorators: \`${typeName(target)}\`.`);
			} else {
				throw new EcceError(`Cannot ${functionName}() an object not created with makeObservable().`);
			}
		}

		if(!key) {
			return rootSubject;
		}

		const targetSubject = rootSubject.getProperty(key);
		if(!targetSubject) {
			if(!(key in target)) {
				throw new EcceError(`Cannot ${functionName}() a non-existant property: \`${rootSubject.getFullPropertyName(key)}\`.`);
			}

			throw new EcceError(`Cannot ${functionName}() a property not decorated with @observable(): \`${rootSubject.getFullPropertyName(key)}\`.`);
		}

		return targetSubject;
	}

	/**
	 * Add an observable property.
	 *
	 * @param key The name of the property.
	 * @returns The Subject instance created for the property.
	 *
	 * @throws if a property has already been created with `key`.
	 */
	addProperty(key: PropertyKey): Subject {
		/* c8 ignore next 5 */
		if(this._properties?.has(key)) {
			throw new EcceError(
				`Duplicate property key: "${this.getFullPropertyName(key)}". This is probably a bug in ecce...`
			);
		}

		const subject = new Subject(this.getFullPropertyName(key), this);
		(this._properties ??= new Map()).set(key, subject);

		return subject;
	}

	/**
	 * Get the Subject which represents an observable property of this Subject,
	 * or null if no property has been added with that `key`.
	 */
	getProperty(key: PropertyKey): Subject | null {
		return this._properties?.get(key) ?? null;
	}

	/**
	 * Add an observer to this Subject. Does nothing if `observer` was already
	 * added to this Subject. Will change the `schedule` of an observer if it is
	 * re-added with a different `schedule`.
	 */
	observe(observer: Observer) {
		(this._observers ??= new ObserverSet(this.name)).add(observer);
	}

	/**
	 * Remove an observer from this subject. Does nothing if `observer` was not
	 * previously added.
	 */
	unobserve(observer: Observer) {
		this._observers?.delete(observer);
	}

	/**
	 * Notify all observers registered to this Subject.
	 */
	notify(): void {
		this._ref = null;
		this._observers?.notify();
		this._owner?.notify();
	}

	getFullPropertyName(key: PropertyKey): string {
		return this.name + '.' + key.toString();
	}

	get ref(): unknown {
		return this._ref ??= Symbol();
	}
}