import { EcceError } from './error';
import { typeName } from './util';


type BoundDecoratorContext = ClassMemberDecoratorContext & {
	private: false;
};

/**
 * Decorator to bind a class' method to that class permanently, allowing it to
 * be used when detached from the class.
 *
 * ```ts
 *   class Foo {
 *     value = 'world';
 *
 *     ＠bound()
 *     greet() {
 *        alert(`Hello, ${this.value}`);
 *     }
 *   }
 *   const foo = new Foo();
 *
 *   document.getElementById('#greetButton').onclick = foo.greet;
 * ```
 */
export const bound = () => (target: Function, context: BoundDecoratorContext): void => {
	context.addInitializer(function(this: any) {
		if(context.private) {
			throw new EcceError(`Cannot apply @bound() to a #private method: \`${typeName(this)}.${context.name.toString()}()\``);
		}

		this[context.name] = target.bind(this);
	});
};