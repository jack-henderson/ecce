/* c8 ignore start */

import { Subject } from './subject';

/**
 * Return the internal {@link Subject} instance.
 *
 * **Subject should not be considered a stable interface.** It is not covered
 * by sem-ver. You will not be fired if you use it, but you are responsible for
 * handling any breakage.
 *
 * It is exposed here for usage when integrating between ecce and other
 * libraries, and should not be needed in applications.
 *
 * @param target The target of observation.
 * @param key The key of target for observation.
 * @param functionName The name of the function the _end user_ invoked which lead to
 *	the subject being retrieved; used in error messages when no subject exists.
 *	For example: "Invalid target for ${functionName}(): ${target} is not an object."
 */
export const __getInternalEcceSubject = (target: object, key: PropertyKey | undefined, functionName: string): Subject => (
	Subject.find(target, key, functionName)
);
