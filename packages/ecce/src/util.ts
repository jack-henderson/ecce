export const typeName = (x: unknown): string => {
	switch(typeof(x)) {
		case 'boolean':
		case 'number':
		case 'bigint':
		case 'string':
		case 'symbol':
		case 'undefined':
			return typeof(x);
		case 'object':
			if(x === null) {
				return 'null';
			}

			if(Array.isArray(x)) {
				return 'array';
			}

			return Object.getPrototypeOf(x)?.constructor?.name ?? 'Object';
		case 'function':
			return x.name || 'function';
	}
};

export const isClassInstance = (x: unknown): boolean => (
	typeof(x) === 'object'
		&& !Array.isArray(x)
		&& Object.getPrototypeOf(x)?.constructor?.name !== 'Object'
);

export const isPropertyKey = (x: unknown): x is PropertyKey => {
	switch(typeof(x)) {
		case 'symbol':
		case 'number':
		case 'string':
			return true;
		default:
			return false;
	}
};

export const getFullPropertyName = (instance: unknown, property: PropertyKey) => (
	typeName(instance) + '.' + property.toString()
);