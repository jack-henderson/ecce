import { Subject } from './subject';


export const observableRef = <T extends object>(target: T, key?: keyof T): unknown => {
	return Subject.find(target, key, 'observableRef').ref;
};