import { EcceError } from './error';
import { Subject } from './subject';


export type MakeObservableOptions = Partial<{
	name: string;
}>;
export const makeObservable = <T extends object>(source: Readonly<T>, options?: MakeObservableOptions): T => {
	const observable: T = { ...source };
	let subject!: Subject; // eslint-disable-line prefer-const

	const proxy = new Proxy(observable, {
		set: (target, key, newValue) => {
			const propertySubject = subject.getProperty(key);
			if(!propertySubject) {
				throw new EcceError(
					`Cannot set unknown property: \`${subject.name}.${key.toString()}\`. Ensure all properties are set when calling makeObservable().`
				);
			}

			target[key as keyof T] = newValue;
			propertySubject.notify();
			return true;
		},
	});

	subject = Subject.getOrCreate(proxy, options?.name ?? 'Object');

	for(const key of Object.keys(observable)) {
		subject.addProperty(key);
	}

	return proxy;
};