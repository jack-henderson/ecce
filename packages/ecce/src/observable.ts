/* eslint-disable prefer-arrow-callback */
import { EcceError } from './error';
import { Subject } from './subject';
import { getFullPropertyName } from './util';


type ObservableDecoratorContext = (
	| ClassAccessorDecoratorContext & { private: false, static: false }
	| ClassSetterDecoratorContext & { static: false }
	| ClassGetterDecoratorContext & { private: false, static: false }
	| ClassDecoratorContext
);

/**
 * Decorator to make a class member or a class itself observable.
 *
 * Can decorate auto-accessors, setters, getters, and classes.
 *
 * ## Accessors
 * Decorate an `accessor` member to make a publicly gettable & settable
 * observable. Observers added for this property will be notified whenever the
 * property is set. Cannot be used on #private accessors.
 *
 * ```ts
 *   class Foo {
 *     ＠observable()
 *     accessor bar = 0;
 *   }
 *
 *   const foo = new Foo();
 *   observe(foo, 'bar', () => console.log(foo.bar));
 *
 *   foo.bar = 10;
 * ```
 *
 *
 * ## Setters
 * Decorate a private `set` member to make an observable which can only be set
 * internally, or to add additional setting logic. Observers added for this
 * property will be notified whenever the property is set.
 *
 * ```ts
 *   class Foo {
 *     #_bar = 0;
 *     get bar() { return this.#_bar; }
 *     ＠observable() set #bar(value: number) { this.#_bar = value; }
 *
 *     randomBar() {
 *       this.#bar = Math.random();
 *     }
 *   }
 *
 *   const foo = new Foo();
 *   observe(foo, 'bar', () => console.log(foo.bar));
 *
 *   foo.randomBar();
 * ```
 *
 *
 * ## Getters
 * Decorate a `get` member to make an observable which can be manually notified
 * via the `notify()` function. Useful when a member changes without being
 * reassigned.
 *
 * ```ts
 *   class Foo {
 *     #values: number[] = [];
 *
 *     ＠observable()
 *     get values(): readonly number[] {
 *       return this.#values;
 *     }
 *
 *     add(value: number) {
 *       this.#values.push(value);
 *       notify(this, 'values');
 *     }
 *   }
 *
 *   const foo = new Foo();
 *   observe(foo, 'values', () => console.log('values:', foo.values));
 *
 *   foo.add(5);
 * ```
 *
 * ## Classes
 * Decorate a `class` to make the class itself observable, without any
 * observable members.
 */
export const observable = () => (target: any, context: ObservableDecoratorContext): any => {
	if(decorateInvalidStaticMember(context)) {
		return;
	}

	switch(context.kind) {
		case 'accessor':
			return decorateAccessor(target, context);
		case 'setter':
			return decorateSetter(target, context);
		case 'getter':
			return decorateGetter(target, context);
		case 'class':
			return decorateClass(target);
		default:
			decorateInvalidLocation(context);
	}
};

const decorateSetter = (target: (value: unknown) => void, context: ClassSetterDecoratorContext): typeof target => {
	const key = typeof(context.name) === 'string' ? context.name.replace('#', '') : context.name;

	context.addInitializer(function(this: any) {
		Subject.getOrCreate(this).addProperty(key);
	});

	return function(this: any, value: unknown) {
		target.call(this, value);
		Subject.get(this)!.getProperty(key)!.notify();  // eslint-disable-line @typescript-eslint/no-non-null-assertion
	};
};

const decorateGetter = (_: any, context: ClassGetterDecoratorContext): void => {
	context.addInitializer(function(this: any) {
		assertNotPrivate(this, context);
		Subject.getOrCreate(this).addProperty(context.name);
	});
};

const decorateAccessor = (_: ClassAccessorDecoratorTarget<any, unknown>, context: ClassAccessorDecoratorContext): ClassAccessorDecoratorResult<any, unknown> => {
	context.addInitializer(function(this: any) {
		assertNotPrivate(this, context);
		Subject.getOrCreate(this).addProperty(context.name);
	});

	const values = new WeakMap<object, unknown>();

	return {
		init(initialValue) {
			values.set(this, initialValue);
		},
		set(nextValue) {
			values.set(this, nextValue);
			Subject.get(this)!.getProperty(context.name)!.notify(); // eslint-disable-line @typescript-eslint/no-non-null-assertion
		},
		get() {
			return values.get(this);
		},
	};
};

const decorateClass = (target: any): any => {
	return new Proxy(target, {
		construct(_, argArray) {
			const instance = new target(...argArray);
			Subject.getOrCreate(instance);

			return instance;
		},
	});
};

const decorateInvalidLocation = (context: ClassMemberDecoratorContext) => {
	context.addInitializer(function() {
		let message = `Cannot use @observable() on a ${context.kind}: \`${getFullPropertyName(this, context.name)}\`.`;

		if(context.kind === 'field') {
			message += ` Use an auto-accessor instead: \`@observable() accessor ${context.name.toString()}\`.`;
		}

		throw new EcceError(message);
	});
};

const decorateInvalidStaticMember = (context: DecoratorContext): boolean => {
	if(context.kind !== 'class' && context.static) {
		context.addInitializer(function(this: any) {
			throw new EcceError(`Cannot use @observable() on a static ${context.kind}: \`${getFullPropertyName(this, context.name)}\`.`);
		});

		return true;
	}

	return false;
};

const assertNotPrivate = (instance: any, context: { name: PropertyKey, kind: string, private: boolean }): void => {
	if(context.private) {
		throw new EcceError(`Cannot use @observable() on a private ${context.kind}: \`${getFullPropertyName(instance, context.name)}\`.`);
	}
};