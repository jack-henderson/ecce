import { bound } from './bound';
import { makeObservable } from './make-observable';

/**
 * **This decorator is no longer required.**
 *
 *  - If your class uses the `@observable` decorator on at least one of its
 *    members, this decorator can be deleted:
 *    ```ts
 *      // Before:
 *      ＠subject()
 *      class Foo {
 *        ＠observable() accessor bar;
 *      }
 *
 *      // After:
 *      class Foo {
 *        ＠observable() accessor bar;
 *      }
 *    ```
 *
 *  - If your class only used the `@subject` decorator, and used `notify()` to
 *    manually notify class listeners, decorate the class with `@observable`:
 *      ```ts
 *      // Before:
 *      ＠subject()
 *      class Foo {
 *        doSomething() {
 *          notify(this);
 *        }
 *      }
 *
 *      // After:
 *      ＠observable()
 *      class Foo {
 *        doSomething() {
 *          notify(this);
 *        }
 *      }
 *    ```
 *
 * @deprecated
 */
export const subject = () => (_target: any, _context: ClassDecoratorContext) => {/**/};


/**
 * **This decorator has been renamed {@link bound | `@bound()`}**
 *
 * ```ts
 *   // Before
 *   ＠subject
 *   class Foo {
 *     ＠callback
 *     bar() {}
 *   }
 *
 *   // After:
 *   class Foo {
 *     ＠bound()
 *     bar() {}
 *   }
 * ```
 *
 * @deprecated
 */
export const callback = bound();

/**
 * **This function has been renamed {@link makeObservable | `makeObservable()`}.**
 *
 * ```ts
 *   // Before
 *   const model = makeSubject({ foo: 5, bar: 5 });
 *
 *   // After
 *   const model = makeObservable({ foo: 5, bar: 5 });
 * ```
 *
 *
 * @deprecated
 */
export const makeSubject = makeObservable;