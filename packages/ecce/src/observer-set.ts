import type { Observer } from '.';
import { Notifier } from './notifier';


/**
 * A collection of {@link Observer}s.
 *
 * @access private
 */
export class ObserverSet implements Iterable<Observer> {

	readonly name: string;

	private readonly _observers = new Set<Observer>();

	constructor(name: string) {
		this.name = name;
	}

	/**
	 * Add an {@link Observer} to the collection.
	 */
	add(observer: Observer): void {
		this._observers.add(observer);
	}

	/**
	 * Remove an {@link Observer} from the collection.
	 * @param observer
	 */
	delete(observer: Observer): void {
		this._observers?.delete(observer);
	}

  [Symbol.iterator](): Iterator<Observer> {
		return this._observers?.values();
  }

	notify(): void {
		Notifier.notify(this);
	}
}