import { Subject } from './subject';

/**
 * Manually trigger a notification for `target`, or one of it's properties,
 * without requiring a property assignment.
 *
 * @param target The observable to notify.
 * @param key Optionally, the property of `target` to notify.
 *
 * @throws If `target` is not observable.
 * @throws If `key` is not an observable property of `target`.
 */
export const notify = <T extends object>(target: T, key?: keyof T): void => {
	Subject.find(target, key, 'notify').notify();
};