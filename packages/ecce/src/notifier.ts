import type { ObserverSet } from './observer-set';
import { EcceError } from './error';


/**
 * Manages notifying observers.
 *
 * @access private
 */
export class Notifier {
	/**
	 * Track the current active notification job.
	 */
	private static _activeJob: Notifier | null = null;

	/**
	 * Observers which have been triggered since this job started.
	 */
	private readonly _queue = new Set<ObserverSet>();

	/**
	 * Observers which have ever been notified by this job.
	 */
	private readonly _trace = new Set<ObserverSet>();

	private constructor() {} // eslint-disable-line @typescript-eslint/no-empty-function

	/**
	 * Add {@link ObserverSet} to be notified in the job.
	 *
	 * @throws If `observers` was already notified by this job.
	 */
	enqueue(observers: ObserverSet): void {
		if(this._trace.has(observers)) {
			throw new EcceError(
				'Recursive observers: ' + this._makeTraceString(observers)
			);
		}

		this._queue.add(observers);
	}

	run(): void {
		this._notify();
	}

	/**
	 * Notify the {@link ObserverSet}s enqueued in the job.
	 */
	private _notify() {
		const setsToNotify = [ ...this._queue ];
		this._queue.clear();

		for(const set of setsToNotify) {
			/*
				Add the ObserverSet to the trace. If we attempt to `enqueue()` the same
				ObserverSet again, an error will be thrown.
			*/
			this._trace.add(set);

			for(const observer of set) {
				observer();

				/*
					If we now have any ObserverSets in the queue, they were enqueued as a
					result of the above callback. Notify them immediately.
				*/
				if(this._queue.size) {
					this._notify();
				}
			}
		}
	}

	private _makeTraceString(finalSet: ObserverSet): string {
		return [ ...this._trace, finalSet ]
			.map(observer => observer.name)
			.join(' -> ');
	}

	/**
	 * Notify an {@link ObserverSet}'s observers.
	 */
	static notify(observers: ObserverSet): void {
		if(this._activeJob) {
			this._activeJob.enqueue(observers);
			return;
		}

		this._activeJob = new Notifier();
		this._activeJob.enqueue(observers);

		try {
			this._activeJob.run();
		} finally {
			this._activeJob = null;
		}
	}
}
