import { Subject } from './subject';


/**
 * Callback function invoked when an observable is notified.
 */
export type Observer = () => void;

const getObserver = (observerOrKey: Observer | PropertyKey, maybeObserver: Observer | undefined): Observer => (
	typeof(observerOrKey) === 'function' ? observerOrKey : maybeObserver as Observer
);

const getKey = (observerOrKey: Observer | PropertyKey): PropertyKey | null => (
	typeof(observerOrKey) === 'function' ? null : observerOrKey
);

/**
 * Start observing `target`. It will be notified whenever any property
 * of `target` is notified.
 *
 * `target` must be either an object returned from `makeObservable()`, or a class
 * decorated with the `@observable()` decorator.
 *
 * @param target The observable to start observing. Must be either an
 * 	object returned from `makeObservable()`, or an instance of a class which uses
 * 	the `@observable` decorator.
 * @param observer The observer callback to be registered.
 *
 * @throws If `target` is not observable
 */
export function observe<T extends object>(target: T, observer: Observer): void;
/**
 * Start observing a property of `target`.
 *
 * @param target The observable to which the property belongs. Must be either an
 * 	object returned from `makeObservable()`, or an instance of a class which uses
 * 	the `@observable` decorator.
 * @param key The property of target to start observing. Must be the name of a
 * 	property of an object returned from `makeObservable()`, or the name of a
 * 	class member decorated with `@observable`.
 * @param observer The observer callback to be added.
 *
 * @throws If `target` is not observable.
 * @throws If `key` is not an observable property of `target`.
 */
export function observe<T extends object>(target: T, key: keyof T, observer: Observer): void;
export function observe(target: any, observerOrKey: Observer | PropertyKey, maybeObserver?: Observer): void {
	const observer = getObserver(observerOrKey, maybeObserver);
	const key = getKey(observerOrKey);
	Subject.find(target, key, 'observe').observe(observer);
}

/**
 * Stop observing `target`. It will no longer be notified.
 *
 * @param target The observable to which the property belongs. Must be either an
 * 	object returned from `makeObservable()`, or an instance of a class which uses
 * 	the `@observable` decorator.
 * @param observer The observer callback to remove.
 *
 * @throws If `target` is not observable
 */
export function unobserve<T extends object>(target: T, observer: Observer): void;
/**
 * Stop observing a property of `target`. It will no longer be notified.
 *
 * @param target The observable to stop observing. Must be either an
 *  object returned from `makeObservable()`, or an instance of a class which uses
 *  the `@observable` decorator.
 * @param key The property of target to stop observing. Must be the name of a
 * 	property of an object returned from `makeObservable()`, or the name of a
 * 	class member decorated with `@observable`.
 * @param observer The observer callback to remove.
 *
 * @throws If `target` is not observable.
 * @throws If `key` is not an observable property of `target`.
 */
export function unobserve<T extends object>(target: T, key: keyof T, observer: Observer): void;
export function unobserve(target: any, observerOrKey: Observer | PropertyKey, maybeObserver?: Observer): void {
	const observer = getObserver(observerOrKey, maybeObserver);
	const key = getKey(observerOrKey);

	Subject.find(target, key, 'unobserve').unobserve(observer);
}