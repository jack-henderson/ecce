**ecce**
========

[![npm](https://img.shields.io/npm/v/ecce)](https://www.npmjs.com/package/ecce)
![NPM](https://img.shields.io/npm/l/ecce)
[![pipeline status](https://gitlab.com/jack.henderson/ecce/badges/develop/pipeline.svg)](https://gitlab.com/jack.henderson/ecce/-/commits/develop) 
[![coverage report](https://gitlab.com/jack.henderson/ecce/badges/develop/coverage.svg)](https://gitlab.com/jack.henderson/ecce/-/commits/develop)
![npm type definitions](https://img.shields.io/npm/types/ecce)

---