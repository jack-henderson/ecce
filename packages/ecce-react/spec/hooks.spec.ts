
import type { ReactWrapper } from 'enzyme';
import { describe, expect, it, beforeEach, afterEach } from 'vitest';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { createElement } from 'react';
import { observable, useObservable } from '../src';


describe('[ecce-react] useObservable()', () => {
	beforeEach(() => {
		configure({ adapter: new Adapter() });
	});

	let wrapper: ReactWrapper;
	afterEach(() => {
		wrapper?.unmount();
	});

	it('rerenders on observed property change', () => {
		class Foo {
			@observable()
			accessor bar = 5;
		}

		const foo = new Foo();

		let numRenders = 0;
		const Component = () => {
			useObservable(foo, 'bar');
			++numRenders;

			return createElement('h1', { children: foo.bar });
		};

		wrapper = mount(createElement(Component, {}));

		expect(numRenders)
			.to.eq(1);

		expect(wrapper.find('h1').text())
			.to.eq('5');

		foo.bar = 10;

		expect(numRenders)
			.to.eq(2);

		expect(wrapper.find('h1').text())
			.to.eq('10');
	});

	it('does not rerender on unobserved property change', () => {
		class Foo {
			@observable()
			accessor bar = 5;

			@observable()
			accessor baz = 0;
		}

		const foo = new Foo();

		let numRenders = 0;
		const Component = () => {
			useObservable(foo, 'bar');
			++numRenders;

			return createElement('h1', { children: foo.bar });
		};

		wrapper = mount(createElement(Component, {}) as any); // eslint-disable-line @typescript-eslint/no-explicit-any

		expect(numRenders)
			.to.eq(1);

		expect(wrapper.find('h1').text())
			.to.eq('5');


		foo.baz = 10;

		expect(numRenders)
			.to.eq(1);

		expect(wrapper.find('h1').text())
			.to.eq('5');
	});

	it('rerenders on observed subject change', () => {
		class Foo {
			@observable()
			accessor bar = 5;
		}

		const foo = new Foo();

		let numRenders = 0;
		const Component = () => {
			useObservable(foo);
			++numRenders;

			return createElement('h1', { children: foo.bar });
		};

		wrapper = mount(createElement(Component, {}) as any); // eslint-disable-line @typescript-eslint/no-explicit-any

		expect(numRenders)
			.to.eq(1);

		expect(wrapper.find('h1').text())
			.to.eq('5');

		foo.bar = 10;

		expect(numRenders)
			.to.eq(2);

		expect(wrapper.find('h1').text())
			.to.eq('10');
	});
});