import { __getInternalEcceSubject } from 'ecce';
import { useSyncExternalStore } from 'use-sync-external-store/shim';


type Store = {
	subscribe: (callback: VoidFunction) => VoidFunction;
	getSnapshot: () => unknown;
};

const storeCache = new WeakMap<object, Store>();

const getStore = (target: object, key: PropertyKey | undefined): Store => {
	const subject = __getInternalEcceSubject(target, key, 'useObservable');

	let store = storeCache.get(subject);
	if(!store) {
		store = {
			subscribe: callback => {
				subject.observe(callback);

				return () => { subject.unobserve(callback); };
			},
			getSnapshot: () => subject.ref,
		};

		storeCache.set(subject, store);
	}

	return store;
};

export const useObservable = <T extends object, K extends keyof T>(subject: T, key?: K): void => {
	const store = getStore(subject, key);
	useSyncExternalStore(store.subscribe, store.getSnapshot);
};