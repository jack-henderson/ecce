import { useObservable } from './hooks';

/**
 * **This hook has been renamed to {@link useObservable | `useObservable()`}**.
 *
 * ```tsx
 *   // Before
 *   const Component = () => {
 *     useSubject(foo, 'bar');
 *
 *     return <p>{ foo.bar }</p>
 *   }
 *
 *   // After
 *   const Component = () => {
 *     useObservable(foo, 'bar');
 *
 *     return <p>{ foo.bar }</p>
 *   }
 * ```
 *
 * @deprecated
 */
export const useSubject = useObservable;