import { describe, expect, it } from 'vitest';
import { useObservable, useSubject } from '../src';


describe('[ecce-react] legacy', () => {
	describe('useSubject()', () => {
		it('is a synonym for useObservable()', () => {
			expect(useSubject)
				.to.eq(useObservable);
		});
	});
});