import { observe, unobserve } from 'ecce';
import { useLayoutEffect, useReducer } from 'preact/hooks';


const useRerender = (): VoidFunction => useReducer(() => Symbol(), null)[1] as VoidFunction;

export const useObservable = <T extends object, K extends keyof T>(subject: T, key?: K): void => {
	const rerender = useRerender();

	useLayoutEffect(() => {
		// Observe a specific key.
		if(key) {
			observe(subject, key, rerender);

			return () => {
				unobserve(subject, key, rerender);
			};
		}

		// Observe entire subject.
		observe(subject, rerender);

		return () => {
			unobserve(subject, rerender);
		};
	}, [subject, key, rerender]);
};