**ecce-preact**
===============

[![NPM](https://img.shields.io/npm/v/ecce-preact)](https://www.npmjs.com/package/ecce-preact)
![NPM](https://img.shields.io/npm/l/ecce-preact)
[![pipeline status](https://gitlab.com/jack.henderson/ecce/badges/develop/pipeline.svg)](https://gitlab.com/jack.henderson/ecce/-/commits/develop) 
[![coverage report](https://gitlab.com/jack.henderson/ecce/badges/develop/coverage.svg)](https://gitlab.com/jack.henderson/ecce/-/commits/develop)
![npm type definitions](https://img.shields.io/npm/types/ecce-preact)

---
