import tsc from 'vitest-tsc';
import { defineConfig } from 'vitest/config'; // eslint-disable-line import/no-unresolved


export default defineConfig({
	test: {
		environment: 'jsdom',
		outputFile: {
			junit: 'spec-report.xml',
		},
		coverage: {
			provider: 'v8',
			'100': true,
			reporter: [
				'text-summary',
				'html',
			],
			all: true,
			exclude: [
				'bin/',
				'.yarn/',
			],
			reportsDirectory: '.coverage',
		},
	},
	plugins: [
		tsc(),
	],
});