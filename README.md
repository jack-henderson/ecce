<center>

**👀 ecce!**
============
[![pipeline status](https://gitlab.com/jack.henderson/ecce/badges/develop/pipeline.svg)](https://gitlab.com/jack.henderson/ecce/-/commits/develop) 
[![coverage report](https://gitlab.com/jack.henderson/ecce/badges/develop/coverage.svg)](https://gitlab.com/jack.henderson/ecce/-/commits/develop)

_A little library to make observables with TypeScript 5.0 decorators._

</center>

--------
Overview
--------

This is mono-repo for developing `ecce`. The project is comprised of three
packages:

 - [**ecce**](./packages/ecce/README.md): the core library.
 - [**ecce-react**](./packages/ecce-react/README.md): Integrates ecce with [React](https://react.dev/).
 - [**ecce-preact**](./packages/ecce-preact/README.md): Integrates ecce with [Preact](https://preactjs.com/).
